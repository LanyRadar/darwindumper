#!/bin/sh

# This is a revised version of DarwinDumper.sh
# Originally by Trauma on 12/08/09.
# Copyright 2010 org.darwinx86.app. All rights reserved.
# Many thanks to JrCs, sonotone, phcoder.
# Additions and revisions since by STLVNUB and blackosx.
#
# Re-structured and further developed by blackosx July 2012 -> November 2013
# Incorporating original DarwinDumper ideas and code.
#
# Thanks to the following users for providing testing/feedback, additional tools, suggestions and help.
# Slice, dmazar, STLVNUB, !Xabbu, THe KiNG, Trauma, JrCs, Kynnder & droplets 

#set -x
#set -u

# =======================================================================================
# INITIALISATION & NON-SPECIFIC ROUTINES
# =======================================================================================

# ---------------------------------------------------------------------------------------
InitialiseBeforeUI()
{
    local cdir="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    cd "$cdir"; cd ..; cd ..
    local resourcesDir="$( pwd )"/Resources
    if [ -d "$resourcesDir" ]; then
        local dataDir="$resourcesDir"/Data
        local driversDir="$resourcesDir"/Drivers
        local imagesDir="$resourcesDir"/Images
        local toolsDir="$resourcesDir"/Tools
        local scriptsDir="$resourcesDir"/Scripts

        # Global Vars
        gDDTmpFolder="/tmp/DarwinDumper" # Temporary working folder.
        gTmpPreLogFile="$gDDTmpFolder"/tmplogfile
        if [ -f "$gDDTmpFolder"/dd_version ]; then
            gtheVersion=$(cat "$gDDTmpFolder"/dd_version)
        else
            gtheVersion="Unknown"
        fi
        ioregViewerDir="$resourcesDir"/IOregViewer
        gTheLoader=""
        gtheprog="DarwinDumper"
        gScriptRunTime=0
        gLoadedVoodooHda=0
        gLoadedRadeonPci=0
        gRootPriv=0
        gUserPrefsFileName="org.tom.DarwinDumper"
        gUserPrefsFile="$HOME/Library/Preferences/$gUserPrefsFileName"
        gAppReportsFolderName="DarwinDumperReports"
        gMyFolder="$dataDir/$gAppReportsFolderName"
        gNvramCallFileList="$dataDir"/nvram_firmware_variable_calls.txt
        gLogIndent="          "
        gCodecID=""

        # Read and set user id and group as they're used in processUserChoices()
        local tmp=$(cat "$gDDTmpFolder"/dd_user)
        theBoss="${tmp%:*}"
        theBossGroup="${tmp#*:}"

        # Global vars for holding which dumps are wanted.
        gCheckBox_acpi=0
        gCheckBox_audioCodec=0
        gCheckBox_biosSystem=0
        gCheckBox_biosVideo=0
        gCheckBox_devprop=0
        gCheckBox_bootLoaderBootSectors=0
        gCheckBox_diskLoaderConfigs=0
        gCheckBox_diskPartitionInfo=0
        gCheckBox_edid=0
        gCheckBox_firmlog=0
        gCheckBox_firmmemmap=0
        gCheckBox_ioreg=0
        gCheckBox_kerneldmesg=0
        gCheckBox_kernelinfo=0
        gCheckBox_kexts=0
        gCheckBox_lspci=0
        gCheckBox_opencl=0
        gCheckBox_rtc=0
        gCheckBox_dmi=0
        gCheckBox_smc=0
        gCheckBox_sysprof=0
        gCheckBox_rcscripts=0
        gCheckBox_nvram=0
        gCheckBox_enablehtml=0
        gCheckBox_collapsetables=0
        gButton_cancel=0
        gButton_runAll=0
        gButton_runSelected=0
        gRadio_privacy=""
        gNoShow=0

        # Resources - Data
        pciids="$dataDir/pci.ids.gz"
        
        # Resources - Drivers
        pciutildrv="$driversDir/DirectHW.kext"
        pciutildrvLeo="$driversDir/Leo/DirectHW.kext"
        voodoohda="$driversDir/VoodooHDA.kext"
        voodoohdaPreML="$driversDir/PreML/VoodooHDA.kext"
        radeonPci="$driversDir/RadeonPCI.kext"
        radeonPciPreML="$driversDir/PreML/RadeonPCI.kext"
        radeonPciLeo="$driversDir/Leo/RadeonPCI.kext"
        
        # Resources - Scripts
        gatherDiskUtilLoaderinfo="$scriptsDir/gatherDiskUtilLoaderinfo.sh"
        generateHTMLreport="$scriptsDir/generateHTMLreport.sh"
        makePrivate="$scriptsDir/privacy.pl"
        
        # Resources - Tools
        bdmesg="$toolsDir/bdmesg"
        smbiosreader="$toolsDir/smbios-reader"
        dmidecode="$toolsDir/dmidecode"
        gfxutil="$toolsDir/gfxutil"
        iasl="$toolsDir/iasl"
        smcutil="$toolsDir/SMC_util3"
        lspci="$toolsDir/lspci"
        sbmm="$toolsDir/FirmwareMemoryMap"
        rtcdumper="$toolsDir/cmosDumperForOsx"
        flashrom="$toolsDir/flashrom"
        lzma="$toolsDir/lzma"
        oclinfo="$toolsDir/oclinfo"
        ediddecode="$toolsDir/edid-decode"
        ioregwv="$toolsDir/ioregwv"
        getcodecid="$toolsDir/getcodecid"
        getcodecidSl="$toolsDir/getcodecidSL"
        getdump="$toolsDir/getdump"
        radeonDump="$toolsDir/RadeonDump"
        radeonDumpLeo="$toolsDir/RadeonDumpLeo"
        radeonDecode="$toolsDir/radeon_bios_decode"
        nvramTool="$toolsDir/nvram"
          
        # UI
        macgap="$resourcesDir/MacGap.app/Contents/MacOS/MacGap"
    else
        echo "DarwinDumper quit because it couldn't find the Resources folder." >> "$gTmpPreLogFile"
        exit 1
    fi
}

# ---------------------------------------------------------------------------------------
processUserChoices()
{        
    # Wait until temporary file exists and is greater than zero in size.
    # If user ran the UI, then this file will be generated from the UI.
    # If user ran from the command line then this file will have been created by the init script.
    while [ ! -s "$gDDTmpFolder"/dd_ui_return ];
    do
        sleep 1
    done
    
    # Read the temp file then remove it.
    oIFS="$IFS"; IFS=$','
    uiReturnArray=( $(cat "$gDDTmpFolder"/dd_ui_return) )
    IFS="$oIFS"

    # Loop through each option
    for (( x=0; x<${#uiReturnArray[@]}; x++ ))
    do
        # Remove unwanted characters and parse results.
        case "${uiReturnArray[$x]##*:}" in
                    "enablehtml")        gCheckBox_enablehtml=1 ;;
                    "collapsetables")    gCheckBox_collapsetables=1 ;;
                    "privacy")           gRadio_privacy="Private" ;;
                    "ArchiveZip")        gRadio_archiveType="Archive.zip" ;;
                    "ArchiveLzma")       gRadio_archiveType="Archive.lzma" ;;
                    "ArchiveNone")       gRadio_archiveType="Archive_None" ;;
                    "acpi")              gCheckBox_acpi=1 ;;
                    "codecid")           gCheckBox_audioCodec=1 ;;
                    "biosSystem")        gCheckBox_biosSystem=1 ;;
                    "biosVideo")         gCheckBox_biosVideo=1 ;;
                    "devprop")           gCheckBox_devprop=1 ;;
                    "diskLoaderConfigs") gCheckBox_diskLoaderConfigs=1 ;;
                    "bootLoaderBootSectors")   gCheckBox_bootLoaderBootSectors=1 ;;
                    "diskPartitionInfo") gCheckBox_diskPartitionInfo=1 ;;
                    "dmi")               gCheckBox_dmi=1 ;;
                    "edid")              gCheckBox_edid=1 ;;
                    "firmlog")           gCheckBox_firmlog=1 ;;
                    "firmmemmap")        gCheckBox_firmmemmap=1 ;;
                    "ioreg")             gCheckBox_ioreg=1 ;;
                    "kerneldmesg")       gCheckBox_kerneldmesg=1 ;;
                    "kernelinfo")        gCheckBox_kernelinfo=1 ;;
                    "kexts")             gCheckBox_kexts=1 ;;
                    "lspci")             gCheckBox_lspci=1 ;;
                    "rcscripts")         gCheckBox_rcscripts=1 ;;
                    "nvram")             gCheckBox_nvram=1 ;;
                    "opencl")            gCheckBox_opencl=1 ;;
                    "rtc")               gCheckBox_rtc=1 ;;
                    "smc")               gCheckBox_smc=1 ;;
                    "sysprof")           gCheckBox_sysprof=1 ;;
                    "noshow")            gNoShow=1 ;;
                    "user_quit")         gButton_cancel=1 # user quit
                                         ;;
                    "death")             exit 1 # UI has completed and closed
                                         ;; 
        esac
    done
}

# ---------------------------------------------------------------------------------------
InitialiseAfterUI()
{
    local rootSystem=$(CheckOsVersion)

    if [ -f "$gDDTmpFolder"/dd_reports_filepath ]; then
        gDumpDir=$( cat "$gDDTmpFolder"/dd_reports_filepath )
    else
        echo "Reports folder not defined."
        exit 1
    fi
    
    # find sequence number from latest directory in the Reports folder.
    local latestDumpDirName=`ls -d "$gDumpDir"/[0-9][0-9][0-9]* 2>/dev/null | tail -n 1`
    if [ -z "$latestDumpDirName" ] || [ "$latestDumpDirName" == "" ]; then
        local lastSequenceNumber="000"
    else
        # Strip all before last forward slash
        lastSequenceNumber="${latestDumpDirName##*/}"
        # Remove all after and including first underscore
        lastSequenceNumber="${lastSequenceNumber%%_*}"
        # Remove any preceding zeros by converting to an actual number
        lastSequenceNumber="$[10#${lastSequenceNumber}]"
        # If user has 999 dumps, then reset to 0 (can't see this ever happening).
        if [ $lastSequenceNumber -eq 999 ]; then
            lastSequenceNumber="000"
        else
            # increment sequence number
            ((lastSequenceNumber++)) 
        fi
    fi

    #Pad as a string length of 3. For example: 006
    lastSequenceNumber=$( printf %03d $lastSequenceNumber )

    # Find Mac Model to add to the folder name.
    local macModel=`/usr/sbin/system_profiler SPHardwareDataType | grep "Model Identifier:"`
    macModel="${macModel##*: }"

    # Create indexed, time stamped folder to hold this dump.
    local timeStamp=`date "+%F_%H-%M-%S"`
    gThisIndexedDumpFolder="${gDumpDir}/${lastSequenceNumber}_${timeStamp}_${macModel}" 
    mkdir "${gThisIndexedDumpFolder}"

    # Get the bootloader type and version
    gTheLoader=$(GetTheLoaderTypeAndVersion)

    # Create dump folder name.
    gFolderName="${gtheprog}_${gtheVersion}_${gTheLoader}_${rootSystem}_${theBoss}"

    # SetMasterDumpFolder
    gMasterDumpFolder="${gThisIndexedDumpFolder}/${gFolderName}"

    # Set temporary directory for copying and loading kexts from.
    gTMPDir=/tmp

    # Set log file path
    gLogFile="$gMasterDumpFolder"/" DarwinDumper_log.txt"

    # Set up dump folder paths.
    gDumpFolderAcpi="$gMasterDumpFolder/ACPI Tables"
    gDumpFolderAcpiAml="$gDumpFolderAcpi/AML"
    gDumpFolderAcpiDsl="$gDumpFolderAcpi/DSL"
    gDumpFolderAudio="$gMasterDumpFolder/Audio"
    gDumpFolderBios="$gMasterDumpFolder/BIOS"
    gDumpFolderBiosSystem="$gDumpFolderBios/System"
    gDumpFolderBiosVideo="$gDumpFolderBios/Video"
    gDumpFolderBootLoader="$gMasterDumpFolder/Boot Loaders"
    gDumpFolderBootLoaderConfigs="$gDumpFolderBootLoader/Configuration Files"
    gDumpFolderBootLoaderDrivers="$gDumpFolderBootLoader/Drivers"
    gDumpFolderBootLog="$gMasterDumpFolder/BootLog"
    gDumpFolderDevProps="$gMasterDumpFolder/Device Properties"
    gDumpFolderDisks="$gMasterDumpFolder/Disks"
    gDumpFolderDiskBootSectors="$gDumpFolderDisks/Boot Sectors"
    gDumpFolderDiskPartitionInfo="$gDumpFolderDisks/Partition Info"
    gDumpFolderDmi="$gMasterDumpFolder/DMI Tables"
    gDumpFolderEdid="$gMasterDumpFolder/EDID"
    gDumpFolderIoreg="$gMasterDumpFolder/IORegistry"
    gDumpFolderKernel="$gMasterDumpFolder/Kernel"
    gDumpFolderKexts="$gMasterDumpFolder/Kexts"
    gDumpFolderLspci="$gMasterDumpFolder/LSPCI"
    gDumpFolderMemory="$gMasterDumpFolder/Memory"
    gDumpFolderNvram="$gMasterDumpFolder/NVRAM"
    gDumpFolderOpenCl="$gMasterDumpFolder/OpenCL"
    gDumpFolderRcScripts="$gMasterDumpFolder/RC Scripts"
    gDumpFolderRtc="$gMasterDumpFolder/RTC"
    gDumpFolderSmc="$gMasterDumpFolder/SMC"
    gDumpFolderSysProf="$gMasterDumpFolder/System Profiler"
    
    # Export dump folder paths to make them available to the other scripts.
    export gDDTmpFolder
    export gLogFile
    export gDumpFolderAcpi
    export gDumpFolderAcpiAml
    export gDumpFolderAcpiDsl
    export gDumpFolderAudio
    export gDumpFolderBios
    export gDumpFolderBiosSystem
    export gDumpFolderBiosVideo
    export gDumpFolderBootLoader
    export gDumpFolderBootLoaderConfigs
    export gDumpFolderBootLoaderDrivers
    export gDumpFolderBootLog
    export gDumpFolderDevProps
    export gDumpFolderDisks
    export gDumpFolderDiskBootSectors
    export gDumpFolderDiskPartitionInfo
    export gDumpFolderDmi
    export gDumpFolderEdid
    export gDumpFolderIoreg
    export gDumpFolderKernel
    export gDumpFolderKexts
    export gDumpFolderLspci
    export gDumpFolderMemory
    export gDumpFolderNvram
    export gDumpFolderOpenCl
    export gDumpFolderRcScripts
    export gDumpFolderRtc
    export gDumpFolderSmc
    export gDumpFolderSysProf

    # Create / Clean temp directory
    if [ -d "$gTMPDir"/DirectHW.kext ];then
       rm -Rf "$gTMPDir"/DirectHW.kext
    fi
}

# ---------------------------------------------------------------------------------------
setPrefsOwnPerms()
{
    if [ -f "$gUserPrefsFile".plist ]; then
        chmod 755 "$gUserPrefsFile".plist
        chown "$theBoss":"$theBossGroup" "$gUserPrefsFile".plist
    fi
}

# ---------------------------------------------------------------------------------------
GetTheLoaderTypeAndVersion()
{
    # Discover current firmware architecture as, for example:
    # Apple's 64-bit efi can launch OS X 10.6 kernel in 32-bit.
    # Note: This is not the kernel architecture.
    local efi=`ioreg -l -p IODeviceTree | grep firmware-abi | awk '{print $5}'`
    local efiBITS="${efi:5:2}"
    if [ "${efiBITS}" == "32" ]; then
    	efiBITS="IA32"
    elif [ "${efiBITS}" == "64" ]; then
       	efiBITS="X64"
    else
    	efiBITS="WhoKnows"   	
    fi
    
    # Discover current bootloader and associated version.
    local gRefitVers=""
    local gOzmosisVers=""
    local theLoader=$(ioreg -l -pIODeviceTree | grep firmware-vendor | awk '{print $5}' | sed 's/_/ /g' | tr -d "<\">" | xxd -r -p)
    case "$theLoader" in
            "CLOVER")              gRefitVers=$( ioreg -lw0 -pIODeviceTree | grep boot-log | tr [[:lower:]] [[:upper:]] )
                                   if [ "$gRefitVers" != "" ]; then
                                       if [[ "$gRefitVers" == *72454649742072657620* ]]; then
                                           gRefitVers=$( echo "$gRefitVers" | tr -d "    |       "boot-log" = <\">" | LANG=C sed -e 's/.*72454649742072657620//' -e 's/206F6E20.*//' | xxd -r -p | sed 's/:/ /g' )
                                       elif [[ "$gRefitVers" == *436C6F7665722072657620* ]]; then
                                           gRefitVers=$( echo "$gRefitVers" | tr -d "    |       "boot-log" = <\">" | LANG=C sed -e 's/.*436C6F7665722072657620//' -e 's/206F6E20.*//' | xxd -r -p | sed 's/:/ /g' )
                                       else
                                           gRefitVers="0000"
                                       fi 
                                   fi
	                               theLoader="Clover_${efiBITS}_${gRefitVers}" ;;
            "American Megatrends") gRefitVers=$( ioreg -lw0 -pIODeviceTree | grep boot-log | tr [[:lower:]] [[:upper:]] )
                                   if [ "$gRefitVers" != "" ]; then
                                       # Check for "rEFIt rev " 
                                       if [[ "$gRefitVers" == *72454649742072657620* ]]; then
                                           gRefitVers=$( echo "$gRefitVers" | tr -d "    |       "boot-log" = <\">" | LANG=C sed -e 's/.*72454649742072657620//' -e 's/206F6E20.*//' | xxd -r -p | sed 's/:/ /g' )
                                           theLoader="AMI_${efiBITS}_${gRefitVers}"
                                       # Check for "Clover rev " 
                                       elif [[ "$gRefitVers" == *436C6F7665722072657620* ]]; then
                                           gRefitVers=$( echo "$gRefitVers" | tr -d "    |       "boot-log" = <\">" | LANG=C sed -e 's/.*436C6F7665722072657620//' -e 's/206F6E20.*//' | xxd -r -p | sed 's/:/ /g' )
                                           theLoader="AMI_${efiBITS}_${gRefitVers}"
                                       # Check for "Ozmosis"
                                       elif [[ "$gRefitVers" == *4F7A6D6F73697320* ]]; then
                                           gOzmosisVers=$( echo "$gRefitVers" | tr -d "    |       "boot-log" = <\">" | LANG=C sed -e 's/.*4F7A6D6F73697320//' -e 's/20.*//' | xxd -r -p | sed 's/:/ /g' )
                                           theLoader="Ozmosis_${gOzmosisVers}"
                                           gRefitVers=""
                                       # If none of the above, set version to 0000
                                       else
                                           theLoader="AMI_${efiBITS}_0000"
                                       fi
	                               fi
	                               ;;
            "")                    theLoader="Unknown_${efiBITS}" ;;
            "Apple")               local tmp=""
                                   tmp=`ioreg -p IODeviceTree | grep RevoEFI`
                                   if [ ! "$tmp" == "" ]; then 
                                       theLoader="RevoBoot_${efiBITS}"
                                   else
                                       theLoader="${theLoader}_${efiBITS}"
                                   fi
                                   ;;
            *)                     local len=$(echo "${#theLoader}")
                                   if [ $len -le 32 ]; then
                                       theLoader="${theLoader}_${efiBITS}"
                                   else
                                       theLoader="${theLoader:0:31}_${efiBITS}"
                                   fi
                                   ;;
            esac
    theLoader=`echo "${theLoader}" | tr ' ' '_' ` # check for spaces in firmware name, now global variable
    echo "$theLoader"
}

# ---------------------------------------------------------------------------------------
CreateDumpDirs()
{
    local dirToMake="$1"

    for dumpDirs in "$dirToMake";
    do
        if [ ! -d "${dumpDirs}" ]; then
    	    mkdir -p "${dumpDirs}"
    	fi
    done
}

# ---------------------------------------------------------------------------------------
CheckOsVersion()
{
    local osVer=`uname -r`
    local osVer=${osVer%%.*}
    local rootSystem=""
    
    if [ "$osVer" == "8" ]; then
	    rootSystem="Tiger"
    elif [ "$osVer" == "9" ]; then
	    rootSystem="LEO"
    elif [ "$osVer" == "10" ]; then
	    rootSystem="SL"
    elif [ "$osVer" == "11" ]; then
	    rootSystem="Lion"
    elif [ "$osVer" == "12" ]; then
	    rootSystem="ML"
	elif [ "$osVer" == "13" ]; then
	    rootSystem="Mav"
	else 
	    rootSystem="Unknown"
    fi
    echo "$rootSystem"  # This line acts as a return to the caller.
}

# ---------------------------------------------------------------------------------------
LoadPciUtilsDriver()
{
    local isLoaded=""
    checkArch=$( uname -a )
    if [[ "$checkArch" == *_64* ]]; then
        cp -r "$pciutildrv" "$gTMPDir"
        local driverName="${pciutildrv##*/}"
    else
        cp -r "$pciutildrvLeo" "$gTMPDir"
        local driverName="${pciutildrvLeo##*/}"
    fi
    isLoaded=$( kextstat -l | egrep "$driverName" )
    if [ "$isLoaded" == "" ]; then
        chmod -R 755 "$gTMPDir/$driverName"
        chown -R 0:0 "$gTMPDir/$driverName"
        echo "${gLogIndent}Loading $driverName" >> "${gLogFile}"
        /sbin/kextload "$gTMPDir/$driverName"
    fi
}

# ---------------------------------------------------------------------------------------
UnloadPciUtilsDriver()
{
    local isLoaded=""
    checkArch=$( uname -a )
    if [[ "$checkArch" == *_64* ]]; then
        local driverName="${pciutildrv##*/}"
    else
        local driverName="${pciutildrvLeo##*/}"
    fi
    isLoaded=$( kextstat -l | egrep "${driverName%*.kext}" )
        if [ ! "$isLoaded" == "" ]; then
        echo "${gLogIndent}Unloading $driverName" >> "${gLogFile}"
        /sbin/kextunload "$gTMPDir/$driverName"
    fi
    if [ -d "$gTMPDir/$driverName" ]; then
        rm -r "$gTMPDir/$driverName"
    fi
}

# ---------------------------------------------------------------------------------------
LoadVoodooHdaDriver()
{
    local checkSystemVersion=""
    
    # Check to see if VoodooHDA is already loaded on the users system
    local isLoaded=$( kextstat -l | egrep "VoodooHDA" )
    if [ "$isLoaded" == "" ]; then
        # It's not currently loaded, so try to load it.
        
        checkSystemVersion=$(CheckOsVersion)
        if [ "$checkSystemVersion" == "Tiger" ] || [ "$checkSystemVersion" == "LEO" ] || [ "$checkSystemVersion" == "SL" ]; then
            cp -R "$voodoohdaPreML" "$gTMPDir" # This is a different version of VoodooHDA.kext
        else
            cp -R "$voodoohda" "$gTMPDir" # This is one version of VoodooHDA.kext
        fi
        chmod -R 755 "$gTMPDir/VoodooHDA.kext"
        chown -R 0:0 "$gTMPDir/VoodooHDA.kext"
        echo "${gLogIndent}Audio: Loading VoodooHDA.kext..." >> "${gLogFile}"
        /sbin/kextload "$gTMPDir/VoodooHDA.kext"
        gLoadedVoodooHda=1 # make a note we loaded it.
    fi
}

# ---------------------------------------------------------------------------------------
UnloadVoodooHdaDriver()
{
    if [ $gLoadedVoodooHda -eq 1 ]; then
        # Only unload if we loaded it.
        
        # Check to see if VoodooHDA is already loaded on the users system
        local isLoaded=$( kextstat -l | egrep "VoodooHDA" )
        if [ ! "$isLoaded" == "" ]; then
            # Yes, it's loaded
            echo "${gLogIndent}Audio: Unloading VoodooHDA.kext" >> "${gLogFile}"
            /sbin/kextunload "$gTMPDir/VoodooHDA.kext"
        fi
        
        # Remove the kext from the temporary location.
        if [ -d "$gTMPDir/VoodooHDA.kext" ]; then
            rm -r "$gTMPDir/VoodooHDA.kext"
        fi
    fi
}

# ---------------------------------------------------------------------------------------
LoadRadeonPciDriver()
{
    local checkSystemVersion=""
       
    # Check to see if RadeonPCI.kext is already loaded on the users system
    local isLoaded=$( kextstat -l | egrep "RadeonPCI" )
    if [ "$isLoaded" == "" ]; then
        # It's not currently loaded, so try to load it.
        
        checkSystemVersion=$(CheckOsVersion)
        if [ "$checkSystemVersion" == "LEO" ]; then
            cp -R "$radeonPciLeo" "$gTMPDir" # This is one version of RadeonPCI.kext
        elif [ "$checkSystemVersion" == "SL" ] || [ "$checkSystemVersion" == "Lion" ] ; then
            cp -R "$radeonPciPreML" "$gTMPDir" # This is another version of RadeonPCI.kext
        else
            cp -R "$radeonPci" "$gTMPDir" # This is a different version of RadeonPCI.kext
        fi

        chmod -R 755 "$gTMPDir/RadeonPCI.kext"
        chown -R 0:0 "$gTMPDir/RadeonPCI.kext"
        echo "${gLogIndent}Loading RadeonPCI.kext" >> "${gLogFile}"
        /sbin/kextload "$gTMPDir/RadeonPCI.kext"
        gLoadedRadeonPci=1 # make a note we loaded it.
    fi
}

# ---------------------------------------------------------------------------------------
UnloadRadeonPciDriver()
{
    if [ $gLoadedRadeonPci -eq 1 ]; then
        # Only unload if we loaded it.
        
        # Check to see if RadeonPCI.kext is already loaded on the users system
        local isLoaded=$( kextstat -l | egrep "RadeonPCI" )
        if [ ! "$isLoaded" == "" ]; then
            # Yes, it's loaded
            echo "${gLogIndent}Unloading RadeonPCI.kext" >> "${gLogFile}"
            /sbin/kextunload "$gTMPDir/RadeonPCI.kext"
        fi
        
        # Remove the kext from the temporary location.
        if [ -d "$gTMPDir/RadeonPCI.kext" ]; then
            rm -r "$gTMPDir/RadeonPCI.kext"
        fi
    fi
}

# ---------------------------------------------------------------------------------------
CheckRoot()
{
    if [ "`whoami`" != "root" ]; then
        #echo "Running this requires you to be root."
        #sudo "$0"
        gRootPriv=0
    else
        gRootPriv=1
    fi
}

# ---------------------------------------------------------------------------------------
CheckSuccess()
{
    local passedPathAndFile="$1"
    if [ ! -f "${passedPathAndFile}" ]; then
         echo "${gLogIndent}Check: ${passedPathAndFile} failed to be created." >> "${gLogFile}"
    fi
}

# ---------------------------------------------------------------------------------------
WriteTimeToLog()
{
    local passedString="$1"
    if [ ! "$passedString" == "" ]; then
        printf '%03ds : %s\n' $(($(date +%s)-gScriptRunTime)) "$passedString" >> "${gLogFile}"
    fi
}

# ---------------------------------------------------------------------------------------
Privatise()
{   
    # ---------------------------------------------------------------------------------------
    CreateMask()
    {
        # This takes a string and creates a masked string by
        # replacing the centre 80% of characters with a *
        # for ACSII strings or 2A for hex strings.
        # The masked string is returned.
        # Thanks for helping with this function JrCs.
        
        local origStr="$1"
        local stringType="$2"
        local maskElement="*"
        if [[ -n "$origStr" ]];then
            tmp=$(( ${#origStr} * 6 ))                 # Multiply length of string by 6 ( so to keep 6% of chars - 3% at start, 3% at end ).
            if [ $(( ${#tmp} -2 )) -lt 0 ]; then       # Add check for short strings which would otherwise cause a substring expression < 0 error.
                tmp=0
            else
                tmp=${tmp:0:$(( ${#tmp} - 2)) }        # Calculate the number of chars at each end of the string to retain.
            fi
            [[ -z "$tmp" ]] && tmp=1                   # Keep at least 1 char at start and end.
            nbToMask=$(( ${#origStr} - ( 2 * $tmp ) )) # Calculate the length of the required mask.
            if [ "$stringType" == "Hex" ]; then        # If hex then halve mask length as mask char will be two hex chars.
                nbToMask=$(( $nbToMask / 2 ))
                maskElement="2A"
            fi
            if [[ $nbToMask -gt 0 ]]; then             # Create mask string.
                mask=$(printf "${maskElement}"'%.0s' $( jot '' 1 $nbToMask ) )
            else
                mask=""
            fi
            echo ${origStr:0:$tmp}${mask}${origStr: -$tmp}  # return final string.
            #echo ${mask}${origStr: -( 2 * $tmp ) } # Mask from front, leaving end as original.
        fi
    }
    
    # ---------------------------------------------------------------------------------------
    CreateFindReplaceString()
    {
        # This takes a string array of original values.
        # Each value then has an associated mask created.
        # A sed find and replace string is created and returned.
        
        local passedValues=( "$@" )
        for (( n=0; n < ${#passedValues[@]}; n++ ))
        do
            passedValuesMask+=( $(CreateMask "${passedValues[$n]}") )
            passedValuesSearchReplaceString="${passedValuesSearchReplaceString}s/${passedValues[$n]}/${passedValuesMask[$n]}/g;"
        done
        echo "$passedValuesSearchReplaceString"
    }
    
    # ---------------------------------------------------------------------------------------
    GetValue()
    {
        # This takes a search item, for example "IOPlatformSerialNumber" and returns
        # a string array with all matching values found within ioreg.
        # It works for items with single or multiple appearances, for example "IOMACAddress".
        
        local keyToGet="$1"
        local planeToUse="$2"
        local keyValueRead=""
        local keyValue=""
        
        if [[ -n "$keyToGet" ]]; then
            # Read key from system as user might not have dumped ioreg to file.
            if [ ! "$keyToGet" == "SystemSerialNumber" ] && [ ! "$planeToUse" == "IODeviceTree" ]; then
                keyValueRead=( $(ioreg -lw0 -p "$planeToUse" | grep "$keyToGet" | tr -d '"' | tr -d '<>' ) )
            else
                # Don't remove the quote marks.
                keyValueRead=( $(ioreg -lw0 -p "$planeToUse" | grep "$keyToGet" | tr -d '<>' ) )    
            fi
            keyValueNumCheck=$(( ${#keyValueRead[@]} -1 ))
            grabNext=0
            for (( n=0; n < ${#keyValueRead[@]}; n++ ))
            do
                if [[ -n "${keyValueRead[$n]}" ]] && [ ! "${keyValueRead[$n]}" == "$keyToGet" ]; then
                    if [ $grabNext -eq 1 ]; then
                        keyValue+=( "${keyValueRead[$n]}" )
                        grabNext=0
                    fi
                    if [ "${keyValueRead[$n]}" == "=" ]; then # Next element will be data we want
                        grabNext=1
                    fi
                fi
            done
            echo "${keyValue[@]}"
        fi
    }
    
    # ---------------------------------------------------------------------------------------
    Cleanup()
    {
        local fileToRemove="$1"
        if [ -f "$fileToRemove"e ]; then
            rm "$fileToRemove"e
        fi
    }
    
    # ---------------------------------------------------------------------------------------
    ApplyMask()
    {
        local origStr="$1"
        local maskedStr="$2"
        local targetFile="$3"
        if [ "$maskedStr" == "" ]; then # Used for pre-constructed search/replace string.
            LANG=C sed -ie "${origStr}" "$targetFile"
        else
            LANG=C sed -ie "s/${origStr}/${maskedStr}/g" "$targetFile"
        fi
        Cleanup "$targetFile"
    }
    
    # ---------------------------------------------------------------------------------------
    PatchFullIoregDump()
    {
        local searchString="$1"
        local replacementString="$2"
        
        # Check Full IOReg Dump
        if [ -d "$gDumpFolderIoreg"/IORegViewer ]; then
            local dirToCheck="$gDumpFolderIoreg"/IORegViewer/Resources/dataFiles
            if [ -d "$dirToCheck" ]; then
        
                # Loop through each sub directory
                local subDirs=($( ls "$dirToCheck" ))
                for (( n=0; n < ${#subDirs[@]}; n++ ))
                do
                    local filesToPatch=()
                    cd "$dirToCheck/${subDirs[$n]}"
                    
                    # Build array of all files to patch for this item.
                    filesToPatch=(`grep -l "$searchString" * `)
                
                    # Loop through array and patch each file.
                    for (( m=0; m < ${#filesToPatch[@]}; m++ ))
                    do
                        # Add for quoted SystemSerialNumber
                        if [ "${filesToPatch[$m]}" ] && [ "$searchString" == "SystemSerialNumber" ] && [[ ! "$replacementString" == *g* ]]; then
                            # Serial number will be like this:  \"A\",\"B\",\"C\",....   replace with \"*\",\"*\",\"*\",.....
                            LANG=C sed -ie 's/\\'"\"${replacementString}"'/\\\"*/g' "$dirToCheck"/"${subDirs[$n]}"/"${filesToPatch[$m]}"
                            break
                        fi

                        if [ "${filesToPatch[$m]}" ]; then
                            ApplyMask "$replacementString" "" "$dirToCheck"/"${subDirs[$n]}"/"${filesToPatch[$m]}"
                        fi
                    done
                    cd ..
                done
            fi
        fi
    }
    
    # ---------------------------------------------------------------------------------------
    maskBootloaderConfigData()
    {
        local passedFileName="$1"
        local passedValueToFind="$2"
        local tmpValue=""
        declare -a tmpFileArray

        local oIFS="$IFS"; IFS=$'\n'
        tmpFileArray=( $(find "$gDumpFolderBootLoaderConfigs" -type f -name "$passedFileName") )
        IFS="$oIFS"
        for (( p=0; p<${#tmpFileArray[@]}; p++ ))
        do
            # Count the number of instances of the search string in file and loop through each.
            # Notes: grep -A places a line containing -- between contiguous groups of matches (returning 3 lines).
            #        So, a single entry will return 2 lines because there are 2 lines (key and string).
            #        Any further entries will return a number in multiples of 3 (+ the original 2 lines).
            #        For example, three occurrences will have the first 2 lines + 2 lots of 3 lines (key, string, --) (8 in total).
            #
            # Search idea taken from Jadran's parse script (find data and read the next line) - Thanks Jadran.
            
            local tmpValue=$( grep -A 1 "<key>${passedValueToFind}</key>" "${tmpFileArray[$p]}" | wc -l )
            for (( o=$tmpValue; o>=2; o=$o-3 )) 
            do
                local tmpValue=$( grep -A 1 "<key>${passedValueToFind}</key>" "${tmpFileArray[$p]}" | head -n $o | tail -2 | sed 1d | sed -e 's/<\/string>//g' )

                # only mask if not already masked (or contains 2 consecutive asterisks)
                if [[ ! "$tmpValue" == *\*\** ]]; then

                    # Remove opening <string>
                    tmpValue=${tmpValue#*<string>}
                    # Remove any trailing whitespace characters
                    tmpValue="${tmpValue%"${tmpValue##*[![:space:]]}"}"
                    if [ ! $tmpValue == "" ]; then
                        local tmpValueSearchReplace=$(CreateFindReplaceString "${tmpValue}") 
                        ApplyMask "$tmpValueSearchReplace" "" "${tmpFileArray[$p]}"
                    fi
                fi
            done 
        done
    }
    
    # ---------------------------------------------------------------------------------------
    SearchAndMask()
    {
        local passedFileToMask="$1"
        local passedStringToFindFirst="$2"
        local passedStringToFindSecond="$3"
        local passedStringToStop="$4"
        
        if [ -f "$passedFileToMask" ]; then
            local foundToken=0
            while read -r lineRead
            do
                if [ $foundToken -eq 2 ] && [ "$lineRead" == "$passedStringToStop" ]; then
                    foundToken=0
                    break
                fi
                if [ $foundToken -eq 2 ]; then
                    local lineMasked=$(CreateMask "$lineRead")
                    ApplyMask "$lineRead" "$lineMasked" "$passedFileToMask"
                fi
                if [ $foundToken -eq 1 ]; then
                    if [[ "$lineRead" == *"${passedStringToFindSecond}"* ]]; then
                        ((foundToken++))
                    fi
                fi
                if [[ "$lineRead" == *"${passedStringToFindFirst}"* ]]; then
                    foundToken=1
                fi
            done < "$passedFileToMask"
        fi
    }
    
    WriteTimeToLog "Making dump(s) private.."
    echo "S:privacy" >> "$gDDTmpFolder"/dd_completed
    
    local targetIOreg="$gDumpFolderIoreg/IOReg.txt"
    local targetIOregDT="$gDumpFolderIoreg/IORegDT.txt"
    local targetSMBIOS="$gDumpFolderDmi/SMBIOS.txt"
    local targetSMBIOSbin="$gDumpFolderDmi/SMBIOS.bin"
    local targetSysProfTxt="$gDumpFolderSysProf/System-Profiler.txt"
    local targetSysProfSpx="$gDumpFolderSysProf/System-Profiler.spx"
    local targetNvramVars="$gDumpFolderNvram/uefi_firmware_vars.txt"
    local targetNvramPlist="$gDumpFolderNvram/nvram.plist"
    local targetNvramHexDump="$gDumpFolderNvram/nvram_hexdump.txt"
    
    local fmmToken=$( echo $(GetValue "fmm-mobileme-token-FMM" "IOService") | sed 's/^ *//g' )
    if [ ! "$fmmToken" == "" ]; then
        local fmmTokenSearchReplace=$(CreateFindReplaceString "${fmmToken[@]}") 
    fi
    local serialNo=$( echo $(GetValue "IOPlatformSerialNumber" "IOService") | sed 's/^ *//g' )
    local serialNoSearchReplace=$(CreateFindReplaceString "${serialNo[@]}") 
    local uuidNo=( $( echo $(GetValue "IOPlatformUUID" "IOService") | sed 's/^ *//g' ) )
    local uuidNoSearchReplace=$(CreateFindReplaceString "${uuidNo[@]}") 
    local macAddress=( $( echo $(GetValue "IOMACAddress" "IOService") | sed 's/^ *//g' ) )
    local macAddressSearchReplace=$(CreateFindReplaceString "${macAddress[@]}")
    local usbSerialNo=( $( echo $(GetValue "USB Serial Number" "IOService") | sed 's/^ *//g' ) ) # iPhone / iPad can be here.
    local usbSerialNoSearchReplace=$(CreateFindReplaceString "${usbSerialNo[@]}") 
    local systemId=( $( echo $(GetValue "system-id" "IODeviceTree") | sed 's/^ *//g' ) )
    local systemIdSearchReplace=$(CreateFindReplaceString "${systemId[@]}")
    local systemSerialNumber=( $( echo $(GetValue "SystemSerialNumber" "IODeviceTree") | sed 's/^ *//g' ) )
    local systemSerialNumberSearchReplace=$(CreateFindReplaceString "${systemSerialNumber[@]}")
    local serialNumber=( $( echo $(GetValue "serial-number" "IODeviceTree") | sed 's/^ *//g' ) )
    local serialNumberSearchReplace=$(CreateFindReplaceString "${serialNumber[@]}")

    # Mask IORegistry text files
    if [ -f "$targetIOreg" ]; then
        if [ ! "$fmmToken" == "" ]; then
            ApplyMask "$fmmTokenSearchReplace" "" "$targetIOreg"
        fi
        ApplyMask "$serialNoSearchReplace" "" "$targetIOreg"
        ApplyMask "$uuidNoSearchReplace" "" "$targetIOreg"
        ApplyMask "$macAddressSearchReplace" "" "$targetIOreg"
        ApplyMask "$usbSerialNoSearchReplace" "" "$targetIOreg"
    fi
    if [ -f "$targetIOregDT" ]; then
        ApplyMask "$serialNoSearchReplace" "" "$targetIOregDT"
        ApplyMask "$uuidNoSearchReplace" "" "$targetIOregDT"
        ApplyMask "$systemIdSearchReplace" "" "$targetIOregDT"
        ApplyMask "$systemSerialNumberSearchReplace" "" "$targetIOregDT"
        ApplyMask "$serialNumberSearchReplace" "" "$targetIOregDT"
    fi
    
    # Mask IORegistry Web Viewer files
    if [ ! "$fmmToken" == "" ]; then
        PatchFullIoregDump "fmm-mobileme-token-FMM" "$fmmTokenSearchReplace"
    fi
    PatchFullIoregDump "IOPlatformSerialNumber" "$serialNoSearchReplace"
    PatchFullIoregDump "IOPlatformUUID" "$uuidNoSearchReplace"
    PatchFullIoregDump "IOMACAddress" "$macAddressSearchReplace"
    PatchFullIoregDump "USB Serial Number" "$usbSerialNoSearchReplace"
    PatchFullIoregDump "system-id" "$systemIdSearchReplace"
    PatchFullIoregDump "serial-number" "$serialNumberSearchReplace"
    PatchFullIoregDump "SystemSerialNumber" "$systemSerialNumberSearchReplace" # may fail... so we do it again differently.
    # Special situation for masking systemSerialNumber in IORegistry Web Viewer.
    # On a real Mac each letter is enclosed in double quotes and these need to be escaped
    # Loop through each character of the serial number
    for (( s=0; s<${#serialNo}; s++ )) 
    do
        local singleChar=${serialNo:$s:1}
        PatchFullIoregDump "SystemSerialNumber" "$singleChar"
    done
    
    # Mask System Profiler files.
    if [ -f "$targetSysProfTxt" ]; then
        ApplyMask "$usbSerialNoSearchReplace" "" "$targetSysProfTxt"
    fi
    if [ -f "$targetSysProfSpx" ]; then
        ApplyMask "$usbSerialNoSearchReplace" "" "$targetSysProfSpx"
    fi
    
    # Mask NVRAM Variables
    if [ ! "$fmmToken" == "" ]; then
        SearchAndMask "$targetNvramPlist" "fmm-mobileme-token-FMM" "<data>" "</data>"
        SearchAndMask "$targetNvramHexDump" "fmm-mobileme-token-FMM" "------------------------------------" ""
    fi
    SearchAndMask "$targetNvramHexDump" "platform-uuid" "------------------------------------" ""
    SearchAndMask "$targetNvramVars" "4D1EDE05-38C7-4A6A-9CC6-4BCCA8B38C14:MLB" "------------------------------------" ""
    SearchAndMask "$targetNvramVars" "4D1EDE05-38C7-4A6A-9CC6-4BCCA8B38C14:ROM" "------------------------------------" ""
    SearchAndMask "$targetNvramVars" "4D1FDA02-38C7-4A6A-9CC6-4BCCA8B30102:SystemSerial" "------------------------------------" ""

    # Mask bootloader configuration files.
    if [ -d "$gDumpFolderBootLoaderConfigs" ]; then
        maskBootloaderConfigData "CurrentCloverBootedConfig.plist" "SerialNumber"
        maskBootloaderConfigData "CurrentCloverBootedConfig.plist" "CustomUUID"
        maskBootloaderConfigData "config.plist" "SerialNumber"
        maskBootloaderConfigData "config.plist" "CustomUUID"
        maskBootloaderConfigData "SMBIOS.plist" "SMserial"
        maskBootloaderConfigData "org.chameleon.Boot.plist" "SystemId"
        maskBootloaderConfigData "settings.plist" "PlatformUUID"
        maskBootloaderConfigData "settings.plist" "SerialNumber" # retain for older versions.
        maskBootloaderConfigData "xpc_smbios.plist" "SerialNumber"
        maskBootloaderConfigData "settings.plist" "MLBData"
    fi
    
    # Mask DMI dump files.
    if [ -f "$targetSMBIOS" ]; then
        ApplyMask "$serialNoSearchReplace" "" "$targetSMBIOS"
    fi  
    if [ -f "$targetSMBIOSbin" ]; then
        ApplyMask "$serialNoSearchReplace" "" "$targetSMBIOSbin"
    fi
    
    # Mask Ozmosis Bootlog
    local checkOzmosisLog=$( find "$gDumpFolderIoreg" -type f -name "*Ozmosis*BootLog.txt" 2>/dev/null )
    if [ ! $checkOzmosisLog == "" ]; then
        ApplyMask "$serialNoSearchReplace" "" "$checkOzmosisLog"
        # Mask custom UUID
        local tmp=$( ioreg -lw0 | grep platform-uuid | tr -d '"' | tr -d '<>' | tr [[:lower:]] [[:upper:]] )
        tmp="${tmp##* }"
        platformUuid="${tmp:6:2}${tmp:4:2}${tmp:2:2}${tmp:0:2}-${tmp:10:2}${tmp:8:2}-${tmp:14:2}${tmp:12:2}-${tmp:16:2}${tmp:18:2}-${tmp:20:12}"
        local platformUuidSearchReplace=$(CreateFindReplaceString "${platformUuid[@]}")
        ApplyMask "$platformUuidSearchReplace" "" "$checkOzmosisLog"
    fi
    
    echo "Completed Privatising Dumps"      
    echo "F:privacy" >> "$gDDTmpFolder"/dd_completed 
}

# ---------------------------------------------------------------------------------------
CloseLog()
{
    gScriptRunTime="$(($(date +%s)-gScriptRunTime))"
    echo "========================================================
DarwinDumper Completed in: ${gScriptRunTime} seconds
========================================================" >> "${gLogFile}"
}

# ---------------------------------------------------------------------------------------
ArchiveDumpFolder()
{
    local prevDateTime
    local existingDumpFolders=()
    local checkIcon=""

    if [ "$gRadio_archiveType" == "Archive.zip" ]; then
        echo "S:archive" >> "$gDDTmpFolder"/dd_completed
        echo "Compressing latest DarwinDumper folder using .zip"
        cd "${gThisIndexedDumpFolder}"
        zip -r -q "${gFolderName}".zip "${gFolderName}"
        sleep 1
        echo "F:archive" >> "$gDDTmpFolder"/dd_completed
    elif [ "$gRadio_archiveType" == "Archive.lzma" ]; then
        echo "S:archive" >> "$gDDTmpFolder"/dd_completed
        echo "Compressing latest DarwinDumper folder using .lzma"
        cd "${gThisIndexedDumpFolder}"
        tar -pvczf "${gFolderName}".tar.gz "${gFolderName}" &> /dev/null
        sleep 1
        "$lzma" e "${gThisIndexedDumpFolder}/${gFolderName}".tar.gz "${gThisIndexedDumpFolder}/${gFolderName}".tar.lzma
        chmod 755 "${gThisIndexedDumpFolder}/${gFolderName}".tar.lzma
        rm "${gThisIndexedDumpFolder}/${gFolderName}".tar.gz
        sleep 1
        echo "F:archive" >> "$gDDTmpFolder"/dd_completed
    fi
}

# ---------------------------------------------------------------------------------------
Finish()
{
    if [ -f "$gDDTmpFolder"/diskutilLoaderInfo.txt ]; then
        rm "$gDDTmpFolder"/diskutilLoaderInfo.txt # Created in the gatherDiskUtilLoaderinfo.sh script
    fi
    
    # Has the user asked not to open the folder and html file? (0 = show, 1 = do not show).
    if [ $gNoShow -ne 1 ]; then
        open "${gThisIndexedDumpFolder}/${gFolderName}"
        if [ -f "${gThisIndexedDumpFolder}/${gFolderName}"/" DarwinDump.htm" ]; then
            open "${gThisIndexedDumpFolder}/${gFolderName}"/" DarwinDump.htm"
        fi
    fi

    # If we created the containing indexed folder when running as root then the owner will be root.
    # Set it to the user which started to process.
    chown -R "${theBoss}":$theBossGroup "${gThisIndexedDumpFolder}"
}

#
# =======================================================================================
# SYSTEM SCAN & DUMP FILE ROUTINES 
# =======================================================================================
#

# ---------------------------------------------------------------------------------------
DumpFilesAcpiTables()
{   
    local acpi_tbls
    local tbl
    local tbl_name
    local tbl_data

    echo "S:acpi" >> "$gDDTmpFolder"/dd_completed
    CreateDumpDirs "$gDumpFolderAcpi"
    acpi_tbls=`ioreg -lw0 | grep "ACPI Tables" | cut -f2 -d"{" | tr "," " "`
    if [ ! "$acpi_tbls" == "" ]; then
        CreateDumpDirs "$gDumpFolderAcpiAml"
        for tbl in $acpi_tbls
        do
            tbl_name=`echo $tbl | cut -f1 -d"=" | tr -d "\""`
            echo "${gLogIndent}Found ACPI table: $tbl_name" >> "${gLogFile}"
            tbl_data=`echo $tbl | cut -f2 -d"<" | tr -d ">"`
            echo $tbl_data | xxd -r -p > "$gDumpFolderAcpiAml"/$tbl_name.aml
            "$iasl" -d "$gDumpFolderAcpiAml"/$tbl_name.aml &> /dev/null
        done
        if [[ `find "$gDumpFolderAcpiAml" -name *.dsl` ]]; then
            CreateDumpDirs "$gDumpFolderAcpiDsl"
            mv "$gDumpFolderAcpiAml"/*.dsl "$gDumpFolderAcpiDsl"/
        fi
    fi
    printf '%03ds : -Completed AcpiTables\n' $(($(date +%s)-gScriptRunTime)) >> "${gLogFile}"
    echo "Completed ACPI Tables"
    echo "F:acpi" >> "$gDDTmpFolder"/dd_completed
}

# ---------------------------------------------------------------------------------------
DumpFilesAudioCodec()
{
    # ---------------------------------------------------------------------------------------
    RunGetCodecID()
    {
        # Check which version of the getcodecid tool to run.
        local checkSystemVersion=$(CheckOsVersion)
        if [ "$checkSystemVersion" == "Tiger" ] || [ "$checkSystemVersion" == "LEO" ]; then
            gCodecID="Not Available"
        elif [ "$checkSystemVersion" == "SL" ]; then
            echo "${gLogIndent}Audio: Running getcodecidSL" >> "${gLogFile}"
            gCodecID=$( "$getcodecidSl" )
        else
            echo "${gLogIndent}Audio: Running getcodecid" >> "${gLogFile}"
            gCodecID=$( "$getcodecid" )
        fi

        if [ ! "$gCodecID" == "" ]; then
            echo "$gCodecID" > "$gDumpFolderAudio"/AudioCodecID.txt
            CheckSuccess "$gDumpFolderAudio/AudioCodecID.txt"
            echo "${gLogIndent}Audio: getcodecid completed audio codec ID dump" >> "${gLogFile}"
        else
            CheckSuccess "$gDumpFolderAudio/AudioCodecID.txt"
            echo "${gLogIndent}Audio: getcodecid failed to dump a codec ID." >> "${gLogFile}"
        fi
    }

    # ---------------------------------------------------------------------------------------
    RunVoodooHDAGetdump()
    {
        echo "${gLogIndent}Audio: Waiting 2 seconds before running VoodooHDA's getdump." >> "${gLogFile}"
        sleep 2
        echo "${gLogIndent}Audio: Running VoodooHDA's getdump..." >> "${gLogFile}"
        voodooDump=`"$getdump"`
        UnloadVoodooHdaDriver
        if [ ! "$voodooDump" == "" ]; then
            echo "${gLogIndent}Audio: VoodooHDA's getdump was successful." >> "${gLogFile}"
            if [[ "$hdaCheck" == *AppleHDA* ]]; then
                echo "${gLogIndent}Audio: VoodooHDA's getdump may produce more info if AppleHDA is disabled." >> "${gLogFile}"
            fi
            echo "$voodooDump" > "$gDumpFolderAudio"/VoodooHDAGetdump.txt        
        else
            echo "${gLogIndent}Audio: VoodooHDA's getdump tool failed to produce a dump." >> "${gLogFile}"
            echo "VoodooHDA's getdump tool failed to produce a dump." >> "$gDumpFolderAudio"/VoodooHDAGetdump.txt  
            if [[ "$hdaCheck" == *AppleHDA* ]]; then
                echo "${gLogIndent}Audio: AppleHDA needs to be disabled for this dump to work properly." >> "${gLogFile}"
                echo "NOTE: AppleHDA was loaded when this dump was attempted." >> "$gDumpFolderAudio"/VoodooHDAGetdump.txt 
                echo "If you wish to get a successful dump using VoodooHDA and it's associated getdump tool then AppleHDA will have to be disabled." >> "$gDumpFolderAudio"/VoodooHDAGetdump.txt
                echo "The simplest option to do that will be to add AppleHDADisabler.kext to your system, rebuild caches and reboot. Then try this dump again." >> "$gDumpFolderAudio"/VoodooHDAGetdump.txt
            else
                echo "NOTE: Apple HDA was not currently loaded at this time." >> "$gDumpFolderAudio"/VoodooHDAGetdump.txt  
            fi
        fi
    }

    echo "S:codecid" >> "$gDDTmpFolder"/dd_completed
    CreateDumpDirs "$gDumpFolderAudio"
    
    # Check if either the AppleHDA or VoodooHDA drivers are loaded.
    # Then run getcodecid tool to scan and print the codecs.
    local hdaCheck=$( kextstat | grep HDA )
    if [[ "$hdaCheck" == *AppleHDA* ]]; then
        echo "${gLogIndent}Audio: AppleHDA is loaded." >> "${gLogFile}"
        RunGetCodecID
    else
        echo "${gLogIndent}Audio: AppleHDA is not loaded." >> "${gLogFile}"
    fi
    
    if [[ "$hdaCheck" == *VoodooHDA* ]]; then
        echo "${gLogIndent}Audio: VoodooHDA is loaded." >> "${gLogFile}"
        RunGetCodecID
    else
        echo "${gLogIndent}Audio: VoodooHDA is not loaded." >> "${gLogFile}"
    fi

    # Try to run VoodooHDA's getdump tool.
    # Ideally this should be run with AppleHDA disabled and not registered in ioreg.

    # If VoodooHDA is not already present then try to load it
    if [[ ! "$hdaCheck" == *VoodooHDA* ]]; then
        echo "${gLogIndent}Audio: Attempting to load VoodooHDA.kext to run the getdump tool." >> "${gLogFile}"
        if [ $gRootPriv -eq 1 ]; then
            LoadVoodooHdaDriver
        else
            echo "${gLogIndent}Audio: ** Root privileges are required to load VoodooHDA.kext." >> "${gLogFile}"
        fi
    fi
      
    # Check again: Is VoodooHDA loaded on the system?
    local hdaCheck=$( kextstat | grep HDA )
    if [[ "$hdaCheck" == *VoodooHDA* ]]; then
        RunVoodooHDAGetdump
    
        # If $gCodecID is not populated then set it from the getdump output.
        if [ "$gCodecID" == "" ]; then
            if [ -f "$gDumpFolderAudio"/VoodooHDAGetdump.txt ]; then
                oIFS="$IFS"; IFS=$'\n'
                local tmp=( $( cat "$gDumpFolderAudio"/VoodooHDAGetdump.txt | grep "HDA Codec #" ))
                for (( c=0; c<${#tmp[@]}; c++ ))
                do
                    gCodecID="$gCodecID"$( printf '%s\r' "${tmp[$c]##*: }" )
                done
                IFS="$oIFS"
                echo "$gCodecID" > "$gDumpFolderAudio"/AudioCodecID.txt
            fi
        fi
    else
        echo "${gLogIndent}Audio: Failed to load VoodooHDA.kext." >> "${gLogFile}"
    fi

    WriteTimeToLog "-Completed DumpFilesAudioCodec"
    echo "F:codecid" >> "$gDDTmpFolder"/dd_completed
}

# ---------------------------------------------------------------------------------------
DumpFilesBiosROM()
{
    echo "S:biosSystem" >> "$gDDTmpFolder"/dd_completed
    CreateDumpDirs "$gDumpFolderBiosSystem"
    if [ $gRootPriv -eq 1 ]; then
        LoadPciUtilsDriver
        "$flashrom" -p internal -r "$gDumpFolderBiosSystem"/biosbackup.rom -o "$gDumpFolderBiosSystem"/flashrom_log.txt &> /dev/null
        CheckSuccess "$gDumpFolderBiosSystem/flashrom_log.txt"
    else
        echo "** Root privileges required to dump system bios." >> "${gLogFile}"
    fi
    WriteTimeToLog "-Completed DumpFilesBiosROM"
    echo "Completed BIOS System ROM"
    echo "F:biosSystem" >> "$gDDTmpFolder"/dd_completed
}

# ---------------------------------------------------------------------------------------
DumpFilesBiosVideoROM()
{
    declare -a vendorIds
    declare -a deviceIds
    declare -a ioregRomDumpHex

    echo "S:biosVideo" >> "$gDDTmpFolder"/dd_completed
    CreateDumpDirs "$gDumpFolderBiosVideo"
    if [ $gRootPriv -eq 1 ]; then

        # Use Andy Vandijck's RadeonDump tool to dump from memory address 0xC0000 which can
        # be the shadow ROM used by the BIOS (PC's only, not real Macs). The tool can dump
        # VBIOS ROM's for ATI, Nvidia and intel.

        # If the kernel is booted in 32-bit mode and the system is a genuine Mac
        # then don't load the driver as it can cause a KP.
        local basicHackCheck=$( ioreg | grep FakeSMC )
        local checkArch=`uname -v`
        checkArch="${checkArch##*_}"
        if [[ "$checkArch" == "I386" ]] && [ "$basicHackCheck" == "" ]; then
            echo "${gLogIndent}Skipping loading RadeonPCIDriver as driver may cause a kernel panic." >> "${gLogFile}"
            numDumpedRoms=0
        else
            LoadRadeonPciDriver

            # The RadeonDump tool saves the ROM to the same DIR it's called from.
            cd "$gDumpFolderBiosVideo"

            local checkSystemVersion=$(CheckOsVersion)
            echo "${gLogIndent}Running RadeonDump" >> "${gLogFile}"
            if [ "$checkSystemVersion" == "Leo" ]; then
            "$radeonDumpLeo" -d &> /dev/null
            else
                "$radeonDump" -d &> /dev/null
            fi
        
            local numDumpedRoms=`ls "$gDumpFolderBiosVideo" | wc -l`
            numDumpedRoms="${numDumpedRoms//[[:space:]]}"
            echo "${gLogIndent}Number of legacy VBIOS ROM's dumped: $numDumpedRoms" >> "${gLogFile}"
        
            UnloadRadeonPciDriver
        fi

        # If there are more GPU's reported by System Profiler than ROM's dumped with Andy's
        # RadeonDump then an attempt is made to extract any ATI legacy VBIOS ROM from IORegistry.

        # Get the system GPUs from System Profiler.
        oIFS="$IFS"; IFS=$'\n'
        vendorIds+=( $(/usr/sbin/system_profiler SPDisplaysDataType | grep "Vendor:" ))
        deviceIds+=( $(/usr/sbin/system_profiler SPDisplaysDataType | grep "Device ID:" ))
        IFS="$oIFS"
        local numGpus=${#vendorIds[@]}

        # Check the number of ROMs dumped against that shown in system profiler. 
        if [ $numGpus -gt $numDumpedRoms ]; then
            # System Profiler shows more GPUs than were dumped - One reason is this could be a real Mac.
            echo "${gLogIndent}Note: System Profiler shows more GPUs than dumped VBIOS ROM's." >> "${gLogFile}"

            # Look for, and save any ATI ROM(s) from IORegistry in to array
            ioregRomDumpHex+=( $( ioreg -lw0 | grep "ATY,bin_image" | sed 's/.*= <//g;s/>//g' ))
            echo "${gLogIndent}Number of ATI ROM images found in IORegistry: ${#ioregRomDumpHex[@]}" >> "${gLogFile}"

            # Loop through array and write any found ROM's to file.
            for (( d=0; d<${#ioregRomDumpHex[@]}; d++ ))
            do
                # Scan ROM for Vendor & Device ID
                # Want to find 0x50434952 (ASCII for PCIR)
                local hexToFind="50434952"
                local hexPosition="${ioregRomDumpHex[$d]%%$hexToFind*}"
                hexPosition="${#hexPosition}"

                # Read the next 4 bytes (taking in to account little endian)
                local romVendorID=${ioregRomDumpHex[$d]:$(($hexPosition+10)):2}${ioregRomDumpHex[$d]:$(($hexPosition+8)):2}
                local romDeviceID=${ioregRomDumpHex[$d]:$(($hexPosition+14)):2}${ioregRomDumpHex[$d]:$(($hexPosition+12)):2}

                local filename="$gDumpFolderBiosVideo"/"$romVendorID"."$romDeviceID".from_ioreg_${d}.rom
                echo "${ioregRomDumpHex[$d]}" | xxd -r -p > "$filename"

                # Check if file was successfully written
                if [ -f "$filename" ]; then
                    echo "${gLogIndent}Extracted legacy VBIOS ROM from IORegistry" >> "${gLogFile}"
                else
                    echo "${gLogIndent}Failed to write $filename to file" >> "${gLogFile}"
                fi
            done
        elif [ $numGpus -lt $numDumpedRoms ]; then
            echo "${gLogIndent}** Note: More VBIOS ROMs were dumped than System Profiler shows!" >> "${gLogFile}"
       fi
       
       # Look for any EFI VBIOS ROMS (Thanks to xsmile for info).
       # The only place to look for now is in an ACPI table named VFCT
       acpiTableToFind="VFCT"

       # Does ACPI Table exist?
       checkTable=$( ioreg -lw0 | grep "$acpiTableToFind" )
       if [ ! "$checkTable" == "" ]; then

           # Find and read ACPI table data
           acpi_tbls=`ioreg -lw0 | grep "ACPI Tables" | cut -f2 -d"{" | tr "," " "`
           if [ ! "$acpi_tbls" == "" ]; then
               for tbl in $acpi_tbls
               do
                   tbl_name=`echo $tbl | cut -f1 -d"=" | tr -d "\""`
                   if [ $tbl_name == "$acpiTableToFind" ]; then
                       echo "${gLogIndent}Found ACPI table: $tbl_name" >> "${gLogFile}"
                       tbl_data=`echo $tbl | cut -f2 -d"<" | tr -d ">"`   
                       break
                   fi
               done
           fi

           # Extract GOP EFI ROM
           if [ ! "$tbl_data" == "" ]; then

               # Check to see if table data contains signature.
               if [[ "$tbl_data" == *55aa* ]]; then
               
                   echo "${gLogIndent}ACPI table $tbl_name contains signature" >> "${gLogFile}"

                   # Scan ROM for Vendor & Device ID
                   hexToFind="55aa"
                   romImage="${tbl_data#*$hexToFind}"
                   romImage="55aa${romImage}"

                   hexToFind="50434952"
                   hexPosition="${romImage%%$hexToFind*}"
                   hexPosition="${#hexPosition}"

                   # Read the next 4 bytes (taking in to account little endian)
                   romVendorID=${romImage:$(($hexPosition+10)):2}${romImage:$(($hexPosition+8)):2}
                   romDeviceID=${romImage:$(($hexPosition+14)):2}${romImage:$(($hexPosition+12)):2}
                   
                   echo "${gLogIndent}Identified ROM VendorID:$romVendorID | DeviceID:$romDeviceID" >> "${gLogFile}"
    
                   filename="$gDumpFolderBiosVideo"/$romVendorID.$romDeviceID.from_ACPI_$acpiTableToFind.rom
                   echo "$romImage" | xxd -r -p > "$filename"
                   
                   # Check if file was successfully written
                   if [ -f "$filename" ]; then
                       echo "${gLogIndent}Extracted VBIOS ROM from ACPI $tbl_name" >> "${gLogFile}"
                   else
                       echo "${gLogIndent}Failed to write $filename to file" >> "${gLogFile}"
                   fi
               else
                   echo "No Signature found"
               fi 
           fi
       #else
           #echo "${gLogIndent}$acpiTableToFind ACPI table not found" >> "${gLogFile}"
       fi

       # Decode any ATI ROM's with Andy's radeon_bios_decode tool.
       videoRomFile=$( find "$gDumpFolderBiosVideo" -type f -name "*.rom" 2>/dev/null )
       for romFile in "$videoRomFile"
       do
           local romFileName="${romFile##*/}"
           # Check to see if this is an ATI ROM dump. If yes, decode it.
           if [ "${romFileName:0:4}" == "1002" ]; then
               if [ -f "$romFile" ]; then
                   "$radeonDecode" < "$romFile" > "$romFile".txt
                   echo "${gLogIndent}Decoded VBIOS ROM file:$romFileName" >> "${gLogFile}"
               fi
           fi
       done

    else
        echo "** Root privileges required to dump video bios." >> "${gLogFile}"
    fi
    WriteTimeToLog "-Completed DumpFilesBiosVideoROM"
    echo "Completed BIOS Video ROM"
    echo "F:biosVideo" >> "$gDDTmpFolder"/dd_completed   
}

# ---------------------------------------------------------------------------------------
DumpFilesDeviceProperties()
{
    echo "S:devprop" >> "$gDDTmpFolder"/dd_completed
    CreateDumpDirs "$gDumpFolderDevProps"
    ioreg -lw0 -p IODeviceTree -n efi -r -x | grep device-properties | sed 's/.*<//;s/>.*//;' | cat > "$gDumpFolderDevProps"/device-properties.hex
    CheckSuccess "$gDumpFolderDevProps"/device-properties.hex
    "$gfxutil" -s -i hex -o xml "$gDumpFolderDevProps"/device-properties.hex "$gDumpFolderDevProps"/device-properties.plist
    CheckSuccess "$gDumpFolderDevProps/device-properties.plist"

    WriteTimeToLog "-Completed DumpFilesDeviceProperties"
    echo "Completed Device Properties"
    echo "F:devprop" >> "$gDDTmpFolder"/dd_completed
}

# ---------------------------------------------------------------------------------------
DumpFilesDiskUtilConfigsAndLoaders()
{
    "$gatherDiskUtilLoaderinfo" "${gMasterDumpFolder}" "$1" "$2" "$3"
    
    # Run Slice's genconfig tool if using Clover rev1672 or newer.
    # The tool is NOT in DarwinDumper but instead will have been installed
    # on the users' system in /usr/bin/local by the Clover installer.
    
    # Find if Clover was used to boot the system
    if [ ! $gRefitVers == "" ] && [ $gRefitVers -gt 1672 ]; then
        
        # Did the user ask to dump the bootloader configs?
        if [ $gCheckBox_diskLoaderConfigs -eq 1 ]; then
        
            # Check directory exists before running binary.
            if [ ! -d "$gDumpFolderBootLoaderConfigs" ]; then
                mkdir -p "$gDumpFolderBootLoaderConfigs"
            fi
            
            if [ -d "$gDumpFolderBootLoaderConfigs" ]; then
                 # Check to see if the tool is installed on the running system
                 if [ -f /usr/local/bin/clover-genconfig ]; then
                     echo "${gLogIndent}/usr/local/bin/clover-genconfig found - dump current Clover boot config" >> "${gLogFile}"
                     /usr/local/bin/clover-genconfig > "$gDumpFolderBootLoaderConfigs"/CurrentCloverBootedConfig.plist
                 else
                     echo "${gLogIndent}/usr/local/bin/clover-genconfig not installed." >> "${gLogFile}"
                 fi
            fi
        fi
    fi

    # ---------------------------------------------------------------------------------------
    BuildBootLoadersFile()
    {
        local passedTextLine="$1"
        if [ ! "$passedTextLine" == "" ]; then
            buildString="${buildString}"$(printf "${passedTextLine}\n")
            buildString="${buildString}\n" 
        fi
    }

    local fileToRead="$gDDTmpFolder"/diskutilLoaderInfo.txt # Created in the gatherDiskUtilLoaderinfo.sh script
    local finalOutFile="$gDumpFolderBootLoader/Boot Loaders.txt"
    buildString=""
    
    if [ $gCheckBox_bootLoaderBootSectors -eq 1 ]; then
        mkdir -p "$gDumpFolderBootLoader"
    fi
    
    if [ -f "$fileToRead" ]; then
        while read -r lineRead
        do
            if [ ! "${lineRead:0:1}" == "=" ]; then
                codeRead="${lineRead%%:*}"
                detailsRead="${lineRead#*:}"
                if [ "$detailsRead" == "" ]; then
                    detailsRead=" "
                fi 
                
                case "$codeRead" in
                    "WD") BuildBootLoadersFile "${detailsRead}@DEVICE@TYPE@NAME@SIZE@MBR (Stage0)@PBR (Stage1)@BootFile (Stage 2)@UEFI BootFile" ;;
                    "DS") diskSize="${detailsRead}" ;;
                    "DT") diskType="${detailsRead}" ;;
                    "S0") stageZero="${detailsRead}" 
                          BuildBootLoadersFile " @ @${diskType}@ @${diskSize}@${stageZero}" ;;
                    "VA") volumeActive="${detailsRead}" ;;
                    "VD") volumeDevice="${detailsRead}" ;;
                    "VT") volumeType="${detailsRead}" ;;
                    "VN") volumeName="${detailsRead}" ;;
                    "VS") volumeSize="${detailsRead}" ;;
                    "S1") stageOne="${detailsRead}"
                          BuildBootLoadersFile "${volumeActive}@${volumeDevice}@${volumeType}@${volumeName}@${volumeSize}@ @${stageOne}" ;;
                    "BF") bootFile="${detailsRead}" ;;
                    "S2") if [ "${detailsRead}" == "" ] || [[ "${detailsRead}" =~ ^\ +$ ]] ;then # if blank or only whitespace
                              stageTwo=""
                          else
                              stageTwo="(${detailsRead})"
                          fi
                          BuildBootLoadersFile " @ @ @ @ @ @ @${bootFile}${stageTwo}";;
                    "UF") uefiFile="${detailsRead}" ;;
                    "U2") if [ "${detailsRead}" == "" ] || [[ "${detailsRead}" =~ ^\ +$ ]] ;then # if blank or only whitespace
                              uefiFileVersion=""
                          else
                              uefiFileVersion="(${detailsRead})"
                          fi
                          BuildBootLoadersFile " @ @ @ @ @ @ @ @${uefiFile}${uefiFileVersion}" ;;
                esac
            fi
        done < "$fileToRead"
        printf "$buildString" | column -t -s@ >> "$finalOutFile"
        
        # Add separator lines in to text file.
        # Find longest line length
        lineLen=$( awk ' { if ( length > x ) { x = length } }END{ print x }' "$finalOutFile" )
        # Create string to insert above title
        topLine=$( printf "="'%.0s' $( jot '' 1 $lineLen ))
        # Create string to insert below title
        botLine=$( printf "–"'%.0s' $( jot '' 1 $lineLen ))
        # Add lines before title line
        LANG=C sed -ie 's/^disk/'"${topLine}"'\'$'\ndisk/g' "$finalOutFile"
        # Add lines after title line
        LANG=C sed -ie 's/BootFile$/BootFile\'$'\n'"${botLine}"'/g' "$finalOutFile"
        # Tidy up
        if [ -f "$finalOutFile"e ]; then
            rm "$finalOutFile"e
        fi
        
        echo "F:bootLoaderBootSectors" >> "$gDDTmpFolder"/dd_completed
    fi
    WriteTimeToLog "-Completed DumpFilesDiskUtilConfigsAndLoaders"
    echo "Completed DiskUtil, Configs and Boot Loaders"
}

# ---------------------------------------------------------------------------------------
DumpFilesFirmwareLog()
{
    # XPC and RevoBoot identify as Apple no need to do anything else
    if [[ ! "${gTheLoader}" =~ "Apple" ]]; then
        echo "S:firmlog" >> "$gDDTmpFolder"/dd_completed
        CreateDumpDirs "$gDumpFolderBootLog"
        "$bdmesg" > "$gDumpFolderBootLog/${gTheLoader}_BootLog.txt"
        CheckSuccess "$gDumpFolderBootLog/${gTheLoader}_BootLog.txt"
    fi

    echo "${gLogIndent}Dumped ${gTheLoader} Firmware log." >> "${gLogFile}"
    WriteTimeToLog "-Completed DumpFilesFirmwareLog"
    echo "Completed Firmware Log"
    echo "F:firmlog" >> "$gDDTmpFolder"/dd_completed
}

# ---------------------------------------------------------------------------------------
DumpFilesFirmwareMemoryMap()
{
    echo "S:firmmemmap" >> "$gDDTmpFolder"/dd_completed
    CreateDumpDirs "$gDumpFolderMemory"
    if [ $gRootPriv -eq 1 ]; then
        "$sbmm" > "$gDumpFolderMemory/FirmwareMemoryMap.txt"
        wait
        CheckSuccess "$gDumpFolderMemory/FirmwareMemoryMap.txt"
    else
        echo "** Root privileges required to dump firmware memory map." >> "${gLogFile}"
    fi
 	WriteTimeToLog "-Completed DumpFilesFirmwareMemoryMap"
    echo "Completed Firmware Memory Map"
    echo "F:firmmemmap" >> "$gDDTmpFolder"/dd_completed
}

# ---------------------------------------------------------------------------------------
DumpFilesIoreg()
{
    local ioregwvSaveDir="/tmp/dataFiles" # This is hardcoded in to the ioregwv binary.
    local ioregServiceDumpFile="$gDumpFolderIoreg"/IORegDump
    local ioregDTDumpFile="$gDumpFolderIoreg"/IORegDTDump
    local ioregACPIDumpFile="$gDumpFolderIoreg"/IORegACPIDump
    local ioregPowerDumpFile="$gDumpFolderIoreg"/IORegPOWERDump
    local ioregUSBDumpFile="$gDumpFolderIoreg"/IORegUSBDump

    echo "S:ioreg" >> "$gDDTmpFolder"/dd_completed
    CreateDumpDirs "$gDumpFolderIoreg"
    
    echo "${gLogIndent}Running ioregwv..." >> "${gLogFile}"
    "$ioregwv" -lw0 -pIOService >/dev/null
    "$ioregwv" -lw0 -pIODeviceTree >/dev/null
    "$ioregwv" -lw0 -pIOACPIPlane >/dev/null
    "$ioregwv" -lw0 -pIOPower >/dev/null
    "$ioregwv" -lw0 -pIOUSB >/dev/null

    # Add necessary scripts to IOReg dump folder.
    cp -R "$ioregViewerDir" "$gDumpFolderIoreg" #/IORegViewer/
    
    # Add created files from /tmp to DarwinDump folder.
    if [ -d "$ioregwvSaveDir" ]; then
        cp -R "$ioregwvSaveDir" "$gDumpFolderIoreg"/IORegViewer/Resources
    
       # Clean up
        rm -rf "$ioregwvSaveDir"
    fi
    
    # Also add normal ioreg text dumps
    ioreg -lw0 > "$gDumpFolderIoreg"/IOReg.txt
    ioreg -lw0 -pIODeviceTree > "$gDumpFolderIoreg"/IORegDT.txt
    
 	WriteTimeToLog "-Completed DumpFilesIoreg"
    echo "Completed Ioreg"
    echo "F:ioreg" >> "$gDDTmpFolder"/dd_completed
}

# ---------------------------------------------------------------------------------------
DumpFilesKernelBootMessages()
{
    echo "S:kerneldmesg" >> "$gDDTmpFolder"/dd_completed
    CreateDumpDirs "$gDumpFolderKernel"
    if [ $gRootPriv -eq 1 ]; then
        /sbin/dmesg > "$gDumpFolderKernel"/Boot-Messages.txt
        CheckSuccess "$gDumpFolderKernel/Boot-Messages.txt"
    else
        echo "** Root privileges required to dump kernel dmesg." >> "${gLogFile}"
    fi
 	WriteTimeToLog "-Completed DumpFilesKernelBootMessages"
    echo "Completed Kernel Boot Messages"
    echo "F:kerneldmesg" >> "$gDDTmpFolder"/dd_completed
}

# ---------------------------------------------------------------------------------------
DumpFilesKernelInfo()
{
    echo "S:kernelinfo" >> "$gDDTmpFolder"/dd_completed
    CreateDumpDirs "$gDumpFolderKernel"
    uname -v | cat > "$gDumpFolderKernel"/Kernel-Info.txt
    /usr/sbin/sysctl -a | grep cpu | cat >> "$gDumpFolderKernel"/Kernel-Info.txt
    /usr/sbin/sysctl -a | grep hw | cat >> "$gDumpFolderKernel"/Kernel-Info.txt
    CheckSuccess "$gDumpFolderKernel/Kernel-Info.txt"
 	WriteTimeToLog "-Completed DumpFilesKernelInfo"
    echo "Completed Kernel Info"
    echo "F:kernelinfo" >> "$gDDTmpFolder"/dd_completed
}

# ---------------------------------------------------------------------------------------
DumpFilesKextLists()
{
    echo "S:kexts" >> "$gDDTmpFolder"/dd_completed
    
    CreateDumpDirs "$gDumpFolderKexts"
    /usr/sbin/kextstat -l | egrep -v "com.apple" > "$gDumpFolderKexts"/NonAppleKexts.txt
    CheckSuccess "$gDumpFolderKexts/NonAppleKexts.txt"   
    echo "${gLogIndent}Dumping Apple Kext list..." >> "${gLogFile}"
    /usr/sbin/kextstat -l | egrep "com.apple" > "$gDumpFolderKexts"/AppleKexts.txt
    CheckSuccess "$gDumpFolderKexts/AppleKexts.txt"    
    WriteTimeToLog "-Completed DumpFilesKextLists"
    echo "Completed Kext Lists"
    echo "F:kexts" >> "$gDDTmpFolder"/dd_completed
}

# ---------------------------------------------------------------------------------------
DumpFilesLspci()
{   
    # ---------------------------------------------------------------------------------------
    Updatepciids()
    {
        local SRC="http://pci-ids.ucw.cz/v2.2/pci.ids.gz"
        local DEST="$pciids"
        local DL
        
        # Update pciids file every week. Check to see if current file
        # is older than seven days - if so, update.
        if [ `find "$DEST" -mtime +7` ]; then
            echo  "${gLogIndent}Update pciids database" >> "${gLogFile}"
            
            # Test server is available
            local testConnection=$(curl --silent --head http://pci-ids.ucw.cz | egrep "OK")
            if [ "$testConnection" ]; then
                echo "${gLogIndent}Update pciids" >> "${gLogFile}"
                if which curl >/dev/null ; then
                	DL="curl -o $DEST $SRC"
                elif which wget >/dev/null ; then
            	    DL="wget -O $DEST $SRC"
                elif which lynx >/dev/null ; then
            	    DL="eval lynx -source $SRC >$DEST"
                else
            	    echo  "${gLogIndent}update-pciids: cannot find curl, wget or lynx" >> "${gLogFile}"
        	        return 1
                fi

                if ! $DL ; then
        	        echo  "${gLogIndent}update-pciids: download failed" >> "${gLogFile}"
        	        rm -f $DEST
        	        return 1
                fi
            else
                echo "${gLogIndent}Note: pciids server not available." >> "${gLogFile}"
            fi
        else
            echo "${gLogIndent}pciids file < 7 days old. No update required." >> "${gLogFile}"
        fi
    }
    
    if [ $gRootPriv -eq 1 ]; then 
        Updatepciids
        if [[ `find "${pciids}"` ]]; then 
   
            echo "${gLogIndent}Dumping LSPCI info..." >> "${gLogFile}"
            echo "S:lspci" >> "$gDDTmpFolder"/dd_completed
            CreateDumpDirs "$gDumpFolderLspci"
            LoadPciUtilsDriver
            "$lspci" -i "$pciids" -nnvv > "$gDumpFolderLspci/lspci (nnvv).txt"
            CheckSuccess "$gDumpFolderLspci/lspci (nnvv).txt"
 	        "$lspci" -i "$pciids" -nnvvbxxxx > "$gDumpFolderLspci/lspci detailed (nnvvbxxxx).txt"
 	        CheckSuccess "$gDumpFolderLspci/lspci detailed (nnvvbxxxx).txt"
	        "$lspci" -i "$pciids" -nnvvt > "$gDumpFolderLspci/lspci tree (nnvvt).txt"
	        CheckSuccess "$gDumpFolderLspci/lspci tree (nnvvt).txt"
        	"$lspci" -i "$pciids" -M > "$gDumpFolderLspci/lspci map (M).txt"
        	CheckSuccess "$gDumpFolderLspci/lspci map (M).txt"
        else
    	    echo "${gLogIndent}Error DumpFilesLspci" >> "${gLogFile}"
        fi
    else
        echo "** Root privileges required to load DirectHW.kext and run lspci." >> "${gLogFile}"
    fi
    WriteTimeToLog "-Completed DumpFilesLspci"
    echo "Completed lspci"
    echo "F:lspci" >> "$gDDTmpFolder"/dd_completed
}

# ---------------------------------------------------------------------------------------
DumpFilesOpenCLInfo()
{
    echo "S:opencl" >> "$gDDTmpFolder"/dd_completed
    CreateDumpDirs "$gDumpFolderOpenCl"
    "$oclinfo" > "$gDumpFolderOpenCl/openCLinfo.txt"
    CheckSuccess "$gDumpFolderOpenCl/openCLinfo.txt"
    WriteTimeToLog "-Completed DumpFilesOpenCLInfo"
    echo "Completed OpenCL Info"
    echo "F:opencl" >> "$gDDTmpFolder"/dd_completed
}

# ---------------------------------------------------------------------------------------
DumpFilesRtc()
{
    echo "S:rtc" >> "$gDDTmpFolder"/dd_completed
    CreateDumpDirs "$gDumpFolderRtc"
    "$rtcdumper" > "$gDumpFolderRtc/RTCDump${rtclength}.txt"
    CheckSuccess "$gDumpFolderRtc/RTCDump${rtclength}.txt"
    wait
    WriteTimeToLog "-Completed DumpFilesRtc"
    echo "Completed RTC"
    echo "F:rtc" >> "$gDDTmpFolder"/dd_completed
}

# ---------------------------------------------------------------------------------------
DumpFilesDmiTables()
{
    echo "S:dmi" >> "$gDDTmpFolder"/dd_completed
    CreateDumpDirs "$gDumpFolderDmi"
    cd "$gDumpFolderDmi"
    "$smbiosreader"
    mv "$gDumpFolderDmi"/dump.bin "$gDumpFolderDmi/SMBIOS.bin"
    CheckSuccess "$gDumpFolderDmi/SMBIOS.bin"
    #local systemVersion=$(CheckOsVersion)
    "$dmidecode" -i "$gDumpFolderDmi/SMBIOS.bin" | cat > "$gDumpFolderDmi/SMBIOS.txt"
    CheckSuccess "$gDumpFolderDmi/SMBIOS.txt"
    WriteTimeToLog "-Completed DumpFilesDmiTables"
    echo "Completed DMI Tables"
    echo "F:dmi" >> "$gDDTmpFolder"/dd_completed
}

# ---------------------------------------------------------------------------------------
DumpFilesSmcKeys()
{
    echo "S:smc" >> "$gDDTmpFolder"/dd_completed
    CreateDumpDirs "$gDumpFolderSmc"
    "$smcutil" -l | cat > "$gDumpFolderSmc/SMC-keys.txt"
    CheckSuccess "$gDumpFolderSmc/SMC-keys.txt"
    "$smcutil" -f | cat > "$gDumpFolderSmc/SMC-fans.txt"
    CheckSuccess "$gDumpFolderSmc/SMC-fans.txt"
    WriteTimeToLog "-Completed DumpFilesSmcKeys"
    echo "Completed SMC Keys"
    echo "F:smc" >> "$gDDTmpFolder"/dd_completed
}

# ---------------------------------------------------------------------------------------
DumpFilesSystemProfilerInfo()
{
    echo "S:sysprof" >> "$gDDTmpFolder"/dd_completed
    CreateDumpDirs "$gDumpFolderSysProf"
    /usr/sbin/system_profiler -xml -detailLevel mini | cat > "$gDumpFolderSysProf"/System-Profiler.spx
    CheckSuccess "$gDumpFolderSysProf/System-Profiler.spx"
    /usr/sbin/system_profiler -detailLevel mini > "$gDumpFolderSysProf"/System-Profiler.txt
    CheckSuccess "$gDumpFolderSysProf/System-Profiler.txt"
    WriteTimeToLog "-Completed DumpFilesSystemProfilerInfo"
    echo "Completed System Profiler"
    echo "F:sysprof" >> "$gDDTmpFolder"/dd_completed
}

# ---------------------------------------------------------------------------------------
DumpFilesRcScripts()
{
    echo "S:rcscripts" >> "$gDDTmpFolder"/dd_completed
    
    # Dump any Clover RC scripts.
    local filesToFind=(rc.local rc.shutdown.local rc.clover.lib)
    for (( f=0; f<${#filesToFind[@]}; f++ ))
    do
        if [ -f /etc/${filesToFind[$f]} ]; then
            CreateDumpDirs "$gDumpFolderRcScripts"
            cp /etc/${filesToFind[$f]} "$gDumpFolderRcScripts/${filesToFind[$f]}"
            CheckSuccess "$gDumpFolderRcScripts/${filesToFind[$f]}"
        fi
    done
    
    local dirsToFind=(rc.boot.d rc.shutdown.d)
    for (( f=0; f<${#dirsToFind[@]}; f++ ))
    do
        if [ -d /etc/${dirsToFind[$f]} ]; then
        CreateDumpDirs "$gDumpFolderRcScripts"
        cp -R /etc/${dirsToFind[$f]} "$gDumpFolderRcScripts"
    fi
    done
    
    WriteTimeToLog "-Completed DumpFilesRcScripts"
    echo "Completed RC Scripts"
    echo "F:rcscripts" >> "$gDDTmpFolder"/dd_completed
}

# ---------------------------------------------------------------------------------------
DumpFilesNvram()
{
    # Apple Specific Vars
    echo "S:nvram" >> "$gDDTmpFolder"/dd_completed
    CreateDumpDirs "$gDumpFolderNvram"
    "$nvramTool" -x -p >"$gDumpFolderNvram"/nvram.plist
    "$nvramTool" -hp >"$gDumpFolderNvram"/nvram_hexdump.txt
    CheckSuccess "$gDumpFolderNvram/nvram.plist"
    echo "Completed NVRAM - Apple specific vars"
    
    # UEFI Firmware Vars
    echo "S:misc" >> "$gDDTmpFolder"/dd_completed
    CreateDumpDirs "$gDumpFolderNvram"
    "$nvramTool" -ha > "$gDumpFolderNvram/uefi_firmware_vars.txt"
    CheckSuccess "$gDumpFolderNvram/nvram.plist"
    
    WriteTimeToLog "-Completed DumpFilesNvram"
    echo "Completed NVRAM - UEFI firmware vars"
    echo "F:nvram" >> "$gDDTmpFolder"/dd_completed
}

# ---------------------------------------------------------------------------------------
DumpFilesEdid()
{
    echo "S:edid" >> "$gDDTmpFolder"/dd_completed
    CreateDumpDirs "$gDumpFolderEdid"

    # check in case there's more than one EDID occurrence in ioreg
    local numEdid=$(/usr/sbin/ioreg -lw0 | grep IODisplayEDID | wc -l | tr -d ' ')
    if [ $numEdid -gt 1 ]; then
        for (( e=1; e<=$numEdid; e++ ))
        do
            local lineWanted="sed -n ${e}p"
            /usr/sbin/ioreg -lw0 | grep IODisplayEDID | sed -ne 's/.*EDID" = <//p' | tr -d '>' | $lineWanted > "$gDumpFolderEdid"/EDID${e}.bin
            "$ediddecode" "$gDumpFolderEdid"/EDID${e}.bin | cat > "$gDumpFolderEdid/EDID${e}.txt"
            CheckSuccess "$gDumpFolderEdid/EDID${e}.txt"
            /usr/bin/xxd -r -p "$gDumpFolderEdid"/EDID${e}.bin > "$gDumpFolderEdid"/EDID${e}.hex
            CheckSuccess "$gDumpFolderEdid/EDID${e}.hex"
        done 
    else
        /usr/sbin/ioreg -lw0 | grep IODisplayEDID | sed -ne 's/.*EDID" = <//p' | tr -d '>' > "$gDumpFolderEdid"/EDID.bin
        "$ediddecode" "$gDumpFolderEdid"/EDID.bin | cat > "$gDumpFolderEdid/EDID.txt"
        CheckSuccess "$gDumpFolderEdid/EDID.txt"
        /usr/bin/xxd -r -p "$gDumpFolderEdid"/EDID.bin > "$gDumpFolderEdid"/EDID.hex
        CheckSuccess "$gDumpFolderEdid/EDID.hex"
    fi

    WriteTimeToLog "-Completed DumpFilesEdid"
    echo "Completed EDID"
    echo "F:edid" >> "$gDDTmpFolder"/dd_completed
}
#
# =======================================================================================
# MAIN
# =======================================================================================
#

InitialiseBeforeUI
processUserChoices
CheckRoot
InitialiseAfterUI

gScriptRunTime="$(date +%s)"

# Call routines to scan system and dump text files.
# as long as the cancel button was not ticked.
if [ $gButton_cancel -eq 0 ]; then
    CreateDumpDirs "$gMasterDumpFolder"
    echo "========================================================" > "${gLogFile}"
    echo "Welcome to DarwinDumper ${gtheVersion}" >> "${gLogFile}"
    echo "$( date )" >> "${gLogFile}"
    tmp=$( /usr/sbin/system_profiler SPSoftwareDataType | grep "System Version:" | sed 's/^ *//g' )
    echo "$tmp" >> "${gLogFile}"
    echo "========================================================" >> "${gLogFile}"
    
    # Append prelogfile (any messages before this point) to the main log.
    if [ -f "$gTmpPreLogFile" ]; then
        echo "Initialisation info" >> "${gLogFile}"
        echo "--------------------------------------------------------" >> "${gLogFile}"
        cat "$gTmpPreLogFile" >> "${gLogFile}"
        echo "--------------------------------------------------------" >> "${gLogFile}"
    fi

    echo "Initiating dumps..." 

    # Run the dumps in stages otherwise the system could grind to a complete halt!
    newProcessCount=0
    if [ $gCheckBox_sysprof -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesSystemProfilerInfo & pidFilesSytemProfilerInfo=$! ; flagSP=0
        WriteTimeToLog "+Started process DumpFilesSystemProfilerInfo: pid $pidFilesSytemProfilerInfo"
    fi
    if [ $gCheckBox_diskLoaderConfigs -eq 1 ] || [ $gCheckBox_bootLoaderBootSectors -eq 1 ] || [ $gCheckBox_diskPartitionInfo -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesDiskUtilConfigsAndLoaders "$gCheckBox_diskLoaderConfigs" "$gCheckBox_bootLoaderBootSectors" "$gCheckBox_diskPartitionInfo" & pidFilesDiskUtilAndLoader=$! ; flagDU=0
        WriteTimeToLog "+Started process DumpFilesDiskUtilConfigsAndLoaders: pid $pidFilesDiskUtilAndLoader"
    fi
    if [ $gCheckBox_biosSystem -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesBiosROM & pidFilesBiosROM=$! ; flagFB=0
        WriteTimeToLog "+Started process DumpFilesBiosROM: pid $pidFilesBiosROM"
    fi
    if [ $gCheckBox_biosVideo -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesBiosVideoROM & pidFilesBiosVideoROM=$! ; flagFV=0
        WriteTimeToLog "+Started process DumpFilesBiosVideoROM: pid $pidFilesBiosVideoROM"
    fi
    if [ $newProcessCount -gt 0 ]; then
        c=0
        while sleep 0.5; do    
            if [ $gCheckBox_sysprof -eq 1 ]; then
                kill -0 $pidFilesSytemProfilerInfo &> /dev/null || if [ $flagSP -eq 0 ]; then ((c++)); flagSP=1; fi
            fi
            if [ $gCheckBox_diskLoaderConfigs -eq 1 ] || [ $gCheckBox_bootLoaderBootSectors -eq 1 ] || [ $gCheckBox_diskPartitionInfo -eq 1 ]; then
                kill -0 $pidFilesDiskUtilAndLoader &> /dev/null || if [ $flagDU -eq 0 ]; then ((c++)); flagDU=1; fi
            fi
            if [ $gCheckBox_biosSystem -eq 1 ]; then
                kill -0 $pidFilesBiosROM &> /dev/null|| if [ $flagFB -eq 0 ]; then ((c++)); flagFB=1; fi
            fi
            if [ $gCheckBox_biosVideo -eq 1 ]; then
                kill -0 $pidFilesBiosVideoROM &> /dev/null|| if [ $flagFV -eq 0 ]; then ((c++)); flagFV=1; fi
            fi
            if [ $c -eq $newProcessCount ]; then
                break
            fi
        done
        #wait
    fi

    newProcessCount=0
    if [ $gCheckBox_acpi -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesAcpiTables & pidFilesAcpiTables=$! ; flagAT=0
        WriteTimeToLog "+Started process DumpFilesAcpiTables: pid $pidFilesAcpiTables"
    fi
    if [ $gCheckBox_audioCodec -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesAudioCodec & pidFilesAudioCodec=$! ; flagAC=0
        WriteTimeToLog "+Started process DumpFilesAudioCodec: pid $pidFilesAudioCodec"
    fi
    if [ $gCheckBox_firmmemmap -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesFirmwareMemoryMap & pidFirmwareMemoryMap=$! ; flagFM=0
        WriteTimeToLog "+Started process DumpFilesFirmwareMemoryMap: pid $pidFirmwareMemoryMap"
    fi
    if [ $gCheckBox_kexts -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesKextLists & pidFilesKextLists=$! ; flagKL=0
        WriteTimeToLog "+Started process DumpFilesKextLists: pid $pidFilesKextLists"
    fi
    if [ $gCheckBox_firmlog -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesFirmwareLog & pidFilesFirmwareLog=$! ; flagFL=0
        WriteTimeToLog "+Started process DumpFilesFirmwareLog: pid $pidFilesFirmwareLog"
    fi
    if [ $gCheckBox_devprop -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesDeviceProperties & pidFilesDeviceProperties=$! ; flagDP=0
        WriteTimeToLog "+Started process DumpFilesDeviceProperties: pid $pidFilesDeviceProperties"
    fi
    if [ $gCheckBox_opencl -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesOpenCLInfo & pidFilesOpenCLInfo=$! ; flagOC=0
        WriteTimeToLog "+Started process DumpFilesOpenCLInfo: pid $pidFilesOpenCLInfo"
    fi
    if [ $gCheckBox_kernelinfo -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesKernelInfo & pidFilesKernelInfo=$! ; flagKI=0
        WriteTimeToLog "+Started process DumpFilesKernelInfo: pid $pidFilesKernelInfo"
    fi
    if [ $gCheckBox_kerneldmesg -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesKernelBootMessages & pidFilesKernelBootMessages=$! ; flagKB=0
        WriteTimeToLog "+Started process DumpFilesKernelBootMessages: pid $pidFilesKernelBootMessages"
    fi
    if [ $gCheckBox_smc -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesSmcKeys & pidFilesSmcKeys=$! ; flagSK=0
        WriteTimeToLog "+Started process DumpFilesSmcKeys: pid $pidFilesSmcKeys"
    fi
    if [ $gCheckBox_rtc -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesRtc & pidFilesRtc=$! ; flagRT=0
        WriteTimeToLog "+Started process DumpFilesRtc: pid $pidFilesRtc"
    fi   
    if [ $gCheckBox_rcscripts -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesRcScripts & pidFilesRcScripts=$! ; flagMS=0
        WriteTimeToLog "+Started process DumpFilesRcScripts: pid $pidFilesRcScripts"
    fi 
    if [ $newProcessCount -gt 0 ]; then    
        c=0
        while sleep 0.5; do      
            if [ $gCheckBox_acpi -eq 1 ]; then
                kill -0 $pidFilesAcpiTables &> /dev/null || if [ $flagAT -eq 0 ]; then ((c++)); flagAT=1; fi
            fi
            if [ $gCheckBox_audioCodec -eq 1 ]; then
                kill -0 $pidFilesAudioCodec &> /dev/null || if [ $flagAC -eq 0 ]; then ((c++)); flagAC=1; fi
            fi
            if [ $gCheckBox_firmmemmap -eq 1 ]; then
                kill -0 $pidFirmwareMemoryMap &> /dev/null || if [ $flagFM -eq 0 ]; then ((c++)); flagFM=1; fi
            fi
            if [ $gCheckBox_kexts -eq  1 ]; then
                kill -0 $pidFilesKextLists &> /dev/null || if [ $flagKL -eq 0 ]; then ((c++)); flagKL=1; fi
            fi
            if [ $gCheckBox_firmlog -eq 1 ]; then
                kill -0 $pidFilesFirmwareLog &> /dev/null || if [ $flagFL -eq 0 ]; then ((c++)); flagFL=1; fi
            fi
            if [ $gCheckBox_devprop -eq 1 ]; then
                kill -0 $pidFilesDeviceProperties &> /dev/null || if [ $flagDP -eq 0 ]; then ((c++)); flagDP=1; fi
            fi
            if [ $gCheckBox_opencl -eq 1 ]; then
                kill -0 $pidFilesOpenCLInfo &> /dev/null || if [ $flagOC -eq 0 ]; then ((c++)); flagOC=1; fi
            fi
            if [ $gCheckBox_kernelinfo -eq 1 ]; then
                kill -0 $pidFilesKernelInfo &> /dev/null || if [ $flagKI -eq 0 ]; then ((c++)); flagKI=1; fi
            fi
            if [ $gCheckBox_kerneldmesg -eq 1 ]; then
                kill -0 $pidFilesKernelBootMessages &> /dev/null || if [ $flagKB -eq 0 ]; then ((c++)); flagKB=1; fi
            fi
            if [ $gCheckBox_smc -eq 1 ]; then
                kill -0 $pidFilesSmcKeys &> /dev/null || if [ $flagSK -eq 0 ]; then ((c++)); flagSK=1; fi
            fi
            if [ $gCheckBox_rtc -eq 1 ]; then
                kill -0 $pidFilesRtc &> /dev/null || if [ $flagRT -eq 0 ]; then ((c++)); flagRT=1; fi
            fi
            if [ $gCheckBox_rcscripts -eq 1 ]; then
                kill -0 $pidFilesRcScripts &> /dev/null || if [ $flagMS -eq 0 ]; then ((c++)); flagMS=1; fi
            fi
            if [ $c -eq $newProcessCount ]; then
                break
            fi
        done
        #wait
    fi

    newProcessCount=0
    if [ $gCheckBox_lspci -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesLspci & pidFilesLspci=$! ; flagLS=0
        WriteTimeToLog "+Started process DumpFilesLspci: pid $pidFilesLspci"
    fi
    if [ $gCheckBox_dmi -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesDmiTables & pidFilesDmiTable=$! ; flagDT=0
        WriteTimeToLog "+Started process DumpFilesDmiTables: pid $pidFilesDmiTable"
    fi
    if [ $gCheckBox_nvram -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesNvram & pidFilesNvram=$! ; flagNP=0
        WriteTimeToLog "+Started process DumpFilesNvram: pid $pidFilesNvram"
    fi
    if [ $gCheckBox_edid -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesEdid & pidFilesEdid=$! ; flagED=0
        WriteTimeToLog "+Started process DumpFilesEdid: pid $pidFilesEdid"
    fi
    if [ $newProcessCount -gt 0 ]; then 
        c=0
        while sleep 0.5; do      
            if [ $gCheckBox_lspci -eq 1 ]; then
                kill -0 $pidFilesLspci &> /dev/null || if [ $flagLS -eq 0 ]; then ((c++)); flagLS=1; fi
            fi
            if [ $gCheckBox_dmi -eq 1 ]; then
                kill -0 $pidFilesDmiTable &> /dev/null || if [ $flagDT -eq 0 ]; then ((c++)); flagDT=1; fi
            fi
            if [ $gCheckBox_nvram -eq 1 ]; then
                kill -0 $pidFilesNvram &> /dev/null || if [ $flagNP -eq 0 ]; then ((c++)); flagNP=1; fi
            fi
            if [ $gCheckBox_edid -eq 1 ]; then
                kill -0 $pidFilesEdid &> /dev/null || if [ $flagED -eq 0 ]; then ((c++)); flagED=1; fi
            fi
            if [ $c -eq $newProcessCount ]; then
                break
            fi
        done
        #wait
    fi

    newProcessCount=0
    if [ $gCheckBox_ioreg -eq 1 ]; then
        ((newProcessCount++))
        DumpFilesIoreg & pidFilesIoreg=$! ; flagIO=0
        WriteTimeToLog "+Started process DumpFilesIoreg: pid $pidFilesIoreg"
    fi
    if [ $newProcessCount -gt 0 ]; then
        c=0
        while sleep 0.5; do      
            if [ $gCheckBox_ioreg -eq 1 ]; then
                kill -0 $pidFilesIoreg &> /dev/null || if [ $flagIO -eq 0 ]; then ((c++)); flagIO=1; fi
            fi
            if [ $c -eq $newProcessCount ]; then
                break
            fi
        done
    fi
    
    if [ "$gRadio_privacy" == "Private" ]; then
        Privatise
    fi

    UnloadPciUtilsDriver

    dumpTime="$(($(date +%s)-gScriptRunTime))"
    echo "========================================================
Dumps complete after: ${dumpTime} seconds
--------------------------------------------------------" >> "${gLogFile}"

    # Did the user request the HTML report?
    if [ $gCheckBox_enablehtml -eq 1 ]; then
    
        # We pass the audio codec to the generateHTMLreport script
        # for adding to the header of the HTML report.
        # If the user chose to dump the audio codec and it was successful
        # then we can read it from the dumped file.
        
        # Did the user request the audio dump?
        if [ $gCheckBox_audioCodec -eq 1 ]; then
        
            if [ -f "$gDumpFolderAudio"/AudioCodecID.txt ]; then
                oIFS="$IFS"; IFS=$'\r\n'
                tmp=($( cat "$gDumpFolderAudio"/AudioCodecID.txt ))
                for (( c=0; c<${#tmp[@]}; c++ ))
                do
                    gCodecID="$gCodecID"$( printf '%s\n\r' "${tmp[$c]##*: }" )
                done
                IFS="$oIFS"
            fi
        else
            # Try finding the info now.
            # This should get the codec ID's in most instances except 
            # OS X 10.6 where getcodecidSL fails for VoodooHDA.
            hdaCheck=$( kextstat | grep HDA )
            if [[ "$hdaCheck" == *AppleHDA* ]] || [[ "$hdaCheck" == *VoodooHDA* ]]; then
                # Check which version of the getcodecid tool to run.
                checkSystemVersion=$(CheckOsVersion)
                if [ "$checkSystemVersion" == "Tiger" ] || [ "$checkSystemVersion" == "LEO" ]; then
                    gCodecID="Not Available"
                elif [ "$checkSystemVersion" == "SL" ]; then
                    # getcodecidSl fails with VoodooHDA driver on OS X 10.6
                    if [[ "$hdaCheck" == *AppleHDA* ]]; then
                        gCodecID=$( "$getcodecidSl" )
                    elif [[ "$hdaCheck" == *VoodooHDA* ]]; then
                        gCodecID="Please choose the Audio dump."
                    fi
                else
                    gCodecID=$( "$getcodecid" )
                fi
            fi
            
            # Do we really need to go down the route of loading VoodooHDA and runnning
            # the getdump tool just to determine the codec ID's?
            # Problem with that is user may not have chosen to run with root privileges.
            # In which case it will fail!
            
            # The above should satisfy most needs, however on 
            #if [ "$gCodecID" == "" ]; then
            #    DumpFilesAudioCodec
            #    # As user didn't ask for the full audio codec dump, then remove file.
            #    if [ -f "$gDumpFolderAudio"/VoodooHDAGetdump.txt ]; then
            #        rm "$gDumpFolderAudio"/VoodooHDAGetdump.txt
            #    fi
            #fi
            if [ "$gCodecID" == "" ]; then
                gCodecID="Not Available"
            fi
        fi
        
        # Did the user request the HTML divs be collapsed?
        if [ $gCheckBox_collapsetables -eq 1 ]; then
            tableState="none"
        else
            tableState="block"
        fi
        echo "S:enablehtml" >> "$gDDTmpFolder"/dd_completed
        # Build the html files.
        "$generateHTMLreport" "$gMasterDumpFolder" "$gtheprog" "$gtheVersion" "$tableState" "$gCodecID" "$gRadio_privacy"
        echo "F:enablehtml" >> "$gDDTmpFolder"/dd_completed
    fi
    
    # Finish up
    if [ $gButton_cancel -eq 0 ]; then
        CloseLog
        ArchiveDumpFolder
        Finish
    
        # To terminate the UI we write the string "Done" to "$gDDTmpFolder"/dd_completed
        echo "Done" >> "$gDDTmpFolder"/dd_completed
        if [ -f "$gDDTmpFolder"/dd_completed ]; then
            chmod 755 "$gDDTmpFolder"/dd_completed
            chown "${theBoss}":"$theBossGroup" "$gDDTmpFolder"/dd_completed
        fi
        exit 0
    fi
fi
